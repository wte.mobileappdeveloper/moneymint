package com.moneymint.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moneymint.R;


public class SelfHistory extends Fragment {
    View rootView;

    LinearLayout today,yesterday_layout;
    public static TextView selfEarned;
    public static RecyclerView self_recyclerview;
    public static LinearLayout self_nodata;
    public SelfHistory()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.fragment_completed_history, container, false);
//        today=rootView.findViewById(R.id.today);

        self_recyclerview=rootView.findViewById(R.id.self_recyclerview);
        self_nodata=rootView.findViewById(R.id.self_nodata_layout);
        selfEarned=rootView.findViewById(R.id.total_selfBalcAmount);

        return rootView;
    }





}
