package com.moneymint.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneymint.R;
import com.moneymint.activities.UpdateProfileDetails;
import com.moneymint.configs.Configs;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.moneymint.configs.Configs.KEY_CLIENT_ID;
import static com.moneymint.configs.Configs.clientId;

public class ProfileFragment extends Fragment {
    private TextView txt_Uname, txt_Uemail, txt_Ucity,pan, txt_UMobile, txt_UAcNumber, txt_UBranchName, txt_BranchAddress, txt_IFSCCode;
    private ImageView btn_edit,profileimg;
    private static final String KEY_USER_ID = "customer_id";
    private String userID;
    private ProgressDialog progressDialog;
    View view;

    Activity activity;


    public ProfileFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ProfileFragment(Activity activity) {

        this.activity=activity;

    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_profile, container, false);

        progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);
        profileimg=view.findViewById(R.id.profile_image);
        txt_Uname =  view.findViewById(R.id.u_name_profile);
        pan=view.findViewById(R.id.user_pan);
        txt_Uemail =  view.findViewById(R.id.user_email_profile);
        txt_Ucity =  view.findViewById(R.id.user_city_profile);
        txt_UMobile =  view.findViewById(R.id.user_mobile_profile);
        txt_UAcNumber =  view.findViewById(R.id.user_ac_no_profile);
        txt_UBranchName =  view.findViewById(R.id.user_bank_name_profile);
        txt_BranchAddress =  view.findViewById(R.id.branch_address_profile);
        txt_IFSCCode =  view.findViewById(R.id.user_ifsc_profile);
        btn_edit =  view.findViewById(R.id.profile_edit);

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inupdate = new Intent(activity, UpdateProfileDetails.class);



                saveAccountDetails(txt_UAcNumber.getText().toString(),
                        txt_UBranchName.getText().toString(),
                        txt_BranchAddress.getText().toString(),
                        txt_IFSCCode.getText().toString(),
                        pan.getText().toString());

                startActivity(inupdate);
            }
        });

        // For Acces User Id
        SharedPreferences sharedPreferences = Objects.requireNonNull(activity).getSharedPreferences("login", Context.MODE_PRIVATE);
        userID = sharedPreferences.getString("User_Id", "");

        //For Call Parsing Method
        userDetailsParse();


        getGender();

        return view;
    }

    private void userDetailsParse() {
        SharedPreferences acShared= Objects.requireNonNull(activity).getSharedPreferences("AC_DETAILS",Context.MODE_PRIVATE);

        txt_UAcNumber.setText(acShared.getString("AC_NUMBER",""));
        txt_UBranchName.setText(acShared.getString("BANK_NAME",""));
        txt_BranchAddress.setText(acShared.getString("BRANCH_ADDRESS",""));
        txt_IFSCCode.setText(acShared.getString("IFSC_CODE",""));
        pan.setText(acShared.getString("PAN",""));
        if (!acShared.getString("AC_NUMBER","").equals("")){
            btn_edit.setVisibility(View.GONE);
        }
//
//
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_UserDetails,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();

                        Log.e("Profile frag resp",response);
                        try {

                            JSONObject jObject = new JSONObject(response);
                            if (jObject.optString("error").equalsIgnoreCase("false") || jObject.optString("error_msg").equalsIgnoreCase("Success")) {
                                //For Get date From Server
                                String _UName = jObject.getString("Name");
                                String _Mobileno = jObject.getString("Mobileno");
                                String _EmailID = jObject.getString("EmailID");
                                String _City = jObject.getString("City");
                                String _AccountNo = jObject.getString("Accountno");
                                String _BankName = jObject.getString("BankName");
                                String _BranchAddress = jObject.getString("BranchAddress");
                                String _IFSCCode = jObject.getString("IFSCCode");
                                String _PAN=jObject.getString("pan_card");


                                if (!_AccountNo.equals("")){
                                    btn_edit.setVisibility(View.GONE);
                                }

                                txt_Uname.setText(_UName);
                                txt_Uemail.setText(_EmailID);
                                txt_UMobile.setText(_Mobileno);
                                txt_Ucity.setText(_City);
                                txt_UAcNumber.setText(_AccountNo);
                                txt_UBranchName.setText(_BankName);
                                txt_BranchAddress.setText(_BranchAddress);
                                txt_IFSCCode.setText(_IFSCCode);

                                pan.setText(_PAN);
                                saveAccountDetails(_AccountNo,_BankName,_BranchAddress,_IFSCCode,_PAN);
                                progressDialog.hide();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("Profile frag exep",e.toString());
                        }


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(activity, "Server is not responding", Toast.LENGTH_SHORT).show();
                        Log.e("Profile frag err",error.toString());
                        progressDialog.hide();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_USER_ID, userID);
                params.put(KEY_CLIENT_ID, clientId(activity));

                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(activity));
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    public void saveAccountDetails(String _acNumber,String _bankName,String _branchAddress, String _ifceCode,String pann)
    {
        SharedPreferences acShared= Objects.requireNonNull(activity).getSharedPreferences("AC_DETAILS",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = acShared.edit();
        editor.putString("AC_NUMBER",_acNumber);
        editor.putString("BANK_NAME",_bankName);
        editor.putString("BRANCH_ADDRESS",_branchAddress);
        editor.putString("IFSC_CODE",_ifceCode);
        editor.putString("PAN",pann);
        editor.apply();

    }


    public void getGender(){

        SharedPreferences myProfilePref = Objects.requireNonNull(activity).getSharedPreferences("login", Context.MODE_PRIVATE);
        String gender = myProfilePref.getString("Gender", "");


        if (gender.equalsIgnoreCase("F"))
        {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                profileimg.setImageDrawable(activity.getDrawable(R.drawable.girl_user));
            }
        }else
        {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                profileimg.setImageDrawable(activity.getDrawable(R.drawable.profile));
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        userDetailsParse();
    }

    @Override
    public void onPause() {
        super.onPause();
        progressDialog.dismiss();
    }
}