package com.moneymint.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneymint.R;
import com.moneymint.configs.Configs;
import com.moneymint.customViews.MyRecyclerView;
import com.moneymint.models.TodayTaskModel;
import com.moneymint.recyclerAdapter.HomeRecyclerAdapter_completedTask;
import com.moneymint.utils.InternetStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.moneymint.configs.Configs.KEY_CLIENT_ID;
import static com.moneymint.configs.Configs.clientId;

public class Frag_CompletedTask extends Fragment {



    private TextView txt_no_task_available;
    private ImageView img_no_task_available;
    public static final String KEY_USER_ID = "customer_id";
    public String _userId;
    View rootView;
    private MyRecyclerView recyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    Activity activity;
    @SuppressLint("ValidFragment")
    public Frag_CompletedTask(Activity activity) {
        this.activity=activity;
    }

    public Frag_CompletedTask() {
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        rootView= inflater.inflate(R.layout.fragment_completed_task, container, false);

        txt_no_task_available =  rootView.findViewById(R.id.txt_no_task_available);
        img_no_task_available =  rootView.findViewById(R.id.img_no_task_available);
        mSwipeRefreshLayout =  rootView.findViewById(R.id.swifeRefresh);
        mSwipeRefreshLayout.setColorSchemeColors(Color.RED, Color.CYAN, Color.BLUE, Color.YELLOW, Color.MAGENTA);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                checkConnection();
            }
        });

        SharedPreferences preferences = Objects.requireNonNull(this.activity).getSharedPreferences("login", Context.MODE_PRIVATE);
        _userId = preferences.getString("User_Id", "");

        recyclerView =  rootView.findViewById(R.id.recyclearvier);
        checkConnection();
        return rootView;

    }

    private void checkConnection() {
        String message;
        Snackbar snackbar;
        int color;
        if (InternetStatus.getInstance(Objects.requireNonNull(activity)).isOnline()) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;
            fetchTodayTask();
        } else {
            message = "Sorry Check Your Internet Connection !!";
            txt_no_task_available.setText(message);
            color = Color.RED;
            mSwipeRefreshLayout.setRefreshing(false);
        }
        snackbar = Snackbar.make(rootView.findViewById(R.id.coordinatorLayout), message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView textView =  sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }
    private void fetchTodayTask() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_CompletedTask,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Frag_completed resp",response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("error").equalsIgnoreCase("true") || jsonObject.optString("error_msg").equalsIgnoreCase("No Task Avaialable !!")) {

//                                txt_no_task_available.setText(jsonObject.optString("error_msg"));
                                txt_no_task_available.setVisibility(View.VISIBLE);
                                img_no_task_available.setVisibility(View.VISIBLE);

                            } else if (jsonObject.optString("error").equalsIgnoreCase("false") || jsonObject.optString("error_msg").equalsIgnoreCase("Task List Available")) {
                                txt_no_task_available.setVisibility(View.INVISIBLE);
                                img_no_task_available.setVisibility(View.INVISIBLE);
                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                ArrayList<TodayTaskModel> list = new ArrayList<>();

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    TodayTaskModel todayTask = new TodayTaskModel();

                                    JSONObject jsonObj = jsonArray.getJSONObject(i);
//                                    String package_ID = jsonObj.getString("Package_ID");
//                                    String workassign_date = jsonObj.getString("Workassign_Date");
//                                    String workassign_By = jsonObj.getString("Workassign_By");
//                                    String workStatus=jsonObj.getString("WorkStatus");
//
//
//                                    String package_Name = jsonObj.getString("Package_Name");
//                                    String Amount_Credit = jsonObj.getString("Amount_Credit");
//                                    String work_url = jsonObj.getString("work_url");
//                                    String webView_url = jsonObj.getString("webUrl");
//                                    String appsdesc = jsonObj.getString("AppSdesc");
//                                    String appdesc = jsonObj.getString("AppDesc");
//                                    String icon = jsonObj.getString("Icon");
//                                    String showPoint= jsonObj.getString("showPoint");
// For Set data
                                    todayTask.setAmount_Credit(jsonObj.getString("pkgcost"));//
                                    todayTask.setPackage_id(jsonObj.getString("campaign_id"));//
                                    todayTask.setPackage_Name(jsonObj.getString("camping_name"));//
//                                        todayTask.setWorkassign_Date(jsonObj.getString("assign_work_date"));//
                                    todayTask.setWebUrl(jsonObj.getString("webUrl"));//
                                    todayTask.setWork_url(jsonObj.getString("com_url"));//
                                    todayTask.setAppDesc(jsonObj.getString("com_sdesc"));//
                                    todayTask.setWorkStatus(jsonObj.getString("assing_work_status"));//

                                    todayTask.setClickBLabel("Complete");
                                    todayTask.setIcon(jsonObj.getString("icone_url"));
                                    todayTask.setAppSdesc(jsonObj.getString("com_desc"));

                                    list.add(todayTask);
                                }
                                // Setup and Handover data to recyclerview

                                HomeRecyclerAdapter_completedTask adapter= new HomeRecyclerAdapter_completedTask(activity, list);

                                recyclerView.setAdapter(adapter);
                                recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                                recyclerView.setHasFixedSize(true);
                                recyclerView.expandFraomCentre();


                            }
                            mSwipeRefreshLayout.setRefreshing(false);

                        } catch (JSONException e) {
                            Log.e("error cmplttask",e.toString());
                            e.printStackTrace();

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(activity, "Server is not Responding !!", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_USER_ID, _userId);
                params.put(KEY_CLIENT_ID, clientId(activity));
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(activity));
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


}
