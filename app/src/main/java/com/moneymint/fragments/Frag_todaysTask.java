package com.moneymint.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.moneymint.activities.EntertainmentActivity;
import com.moneymint.activities.Home;
import com.moneymint.R;
import com.moneymint.activities.QuizRules;
import com.moneymint.activities.TrendingNewListActivity;
import com.moneymint.activities.ViralNewListActivity;
import com.moneymint.activities.ViralNewsActivity;
import com.moneymint.configs.Configs;
import com.moneymint.customViews.MyRecyclerView;
import com.moneymint.models.TodayTaskModel;
import com.moneymint.models.ViralNewsModel;
import com.moneymint.recyclerAdapter.HomeRecyclerAdapter_TodayTasks;
import com.moneymint.recyclerAdapter.HomeRecyclerAdapter_viralNews;
import com.moneymint.utils.InternetStatus;
import com.startapp.android.publish.adsCommon.StartAppAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static com.moneymint.activities.ChangePassword.KEY_CUSTMRID;
import static com.moneymint.configs.Configs.KEY_CLIENT_ID;
import static com.moneymint.configs.Configs.clientId;


public class Frag_todaysTask extends Fragment {
    private Activity activity;
    MyRecyclerView today_task_homeRecycler;
    RecyclerView viral_news_homeRecycler;
    ImageView trophy;
    private Handler mHandler;
    private TextView txt_no_task_available;
    private ImageView img_no_task_available;
    Button quiz;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressDialog progressDialog;
    SharedPreferences myPrefsNews;
    ScrollView scroll;
    CardView newscard;
    ImageView imgSpecilize,imgReward,imgBonus;
    public String _userId;
    int focus;


    public  Frag_todaysTask(){

    }

    public static Frag_todaysTask getInstance()
    {
        return new Frag_todaysTask();
    }

    @SuppressLint("ValidFragment")
    public Frag_todaysTask(int focus,Activity activity) {
        this.activity=activity;
        this.focus=focus;

    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_today_task, container, false);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        today_task_homeRecycler=view.findViewById(R.id.home_recycler);
        viral_news_homeRecycler=view.findViewById(R.id.viralnews_recycler);
        trophy=view.findViewById(R.id.trophy);
        mHandler = new Handler();
        quiz=view.findViewById(R.id.quizlayout);
        txt_no_task_available =  view.findViewById(R.id.txt_no_task_available);
        img_no_task_available =  view.findViewById(R.id.img_no_task_available);
        newscard=view.findViewById(R.id.newscard);
        scroll= view.findViewById(R.id.scroll);

        if (focus==1){
            Log.e("true","true");
            viral_news_homeRecycler.setFocusable(true);
            viral_news_homeRecycler.setFocusableInTouchMode(true);
            viral_news_homeRecycler.requestFocus();
            view.findViewById(R.id.scroll).scrollTo(0, viral_news_homeRecycler.getBottom());
        }



//        /////////////////////shared pref///////////////////////////////

        SharedPreferences preferences = this.activity.getSharedPreferences("login", Context.MODE_PRIVATE);
        _userId = preferences.getString("User_Id", "");

//        today_task_homeRecycler.setNestedScrollingEnabled(false);
        mSwipeRefreshLayout =  view.findViewById(R.id.mainLAyout);
        mSwipeRefreshLayout.setColorSchemeColors(Color.RED, Color.CYAN, Color.BLUE, Color.YELLOW, Color.MAGENTA);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);


                checkConnection();
            }
        });

        //////////////enter quiz///////////////////
        quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(activity, QuizRules.class));
            }
        });

        imgSpecilize=view.findViewById(R.id.imgSpecilize);



        imgSpecilize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    startActivity(new Intent(getActivity(), EntertainmentActivity.class));




            }
        });





        imgReward=view.findViewById(R.id.imgReward);
        imgReward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//
                    startActivity(new Intent(getActivity(), TrendingNewListActivity.class));
//



            }
        });






        imgBonus=view.findViewById(R.id.imgBonus);
        imgBonus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), TrendingNewListActivity.class));


            }
        });


        new CountDownTimer(10000, 1000){

            public void onTick(long millisUntilFinished){

            }
            public  void onFinish(){

            }
        }.start();


/////////////////////////trophy move///////////////////////////
        startRepeatingTask();

        ///////////////////////////todays task///////////////////
        assignNewTask();


        //////////////load news////////////////////////
        loadViralNews();


        return view;
    }

/////////////check connection///////////////////////
    private void checkConnection() {
        String message;
        Snackbar snackbar;
        int color;
        if (InternetStatus.getInstance(activity).isOnline()) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;
//            getCountParse();
//            updatePlayerId();
            assignNewTask();
            loadViralNews();
        } else {
            message = "Sorry Check Your Internet Connection !!";
            txt_no_task_available.setText(message);

            mSwipeRefreshLayout.setRefreshing(false);

        }
;
    }



    //-----------------------------assign new task aPI---------------------------------
    private void assignNewTask() {
        progressDialog.show();



        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_newTaskAssign,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        Log.e("Frag_tdyTsk assgn resp",response);
//                    progressDialog.dismiss();
//                    mSwipeRefreshLayout.setRefreshing(false);


                        fetchTodayTask();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//            mSwipeRefreshLayout.setRefreshing(false);
                fetchTodayTask();
//            progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_CUSTMRID, _userId);
                params.put(KEY_CLIENT_ID, clientId(activity));
                Log.e("Frag_todayTsk parms",params.toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


    //------------------------------------fetch todays task------------------------------
    private void fetchTodayTask() {
//        progressDialog.show();



        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_workAssign,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        Log.e("Frag_todayTsk resp",response);
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);

                        try {
                            ArrayList<TodayTaskModel> list = new ArrayList<>();

                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("error").equalsIgnoreCase("true") || jsonObject.optString("error_msg").equalsIgnoreCase("No Task Avaialable !!")) {
                                list.clear();
                                Home.today_task_point.setText("0.00");

                                txt_no_task_available.setVisibility(View.VISIBLE);
                                txt_no_task_available.setText("No Task available !!");
                                img_no_task_available.setVisibility(View.VISIBLE);

                            } else if (jsonObject.optString("error").equalsIgnoreCase("false") || jsonObject.optString("error_msg").equalsIgnoreCase("Task List Available")) {
                                txt_no_task_available.setVisibility(View.GONE);
                                img_no_task_available.setVisibility(View.GONE);
                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                list.clear();
                                double totalCount=0.0f;


                                for (int i = 0; i < jsonArray.length(); i++) {

                                    TodayTaskModel todayTask = new TodayTaskModel();

                                    JSONObject jsonObj = jsonArray.getJSONObject(i);

// For Set data

                                    todayTask.setAmount_Credit(jsonObj.getString("pkgcost"));//
                                    todayTask.setPackage_id(jsonObj.getString("campaign_id"));//
                                    todayTask.setPackage_Name(jsonObj.getString("camping_name"));//
//                                    todayTask.setWorkassign_Date(jsonObj.getString("assign_work_date"));//
                                    todayTask.setWork_url(jsonObj.getString("com_url"));//
                                    todayTask.setWebUrl(jsonObj.getString("webUrl"));//
                                    todayTask.setAppDesc(jsonObj.getString("com_desc"));//
                                    todayTask.setAppSdesc(jsonObj.getString("com_sdesc"));
                                    todayTask.setAppDesc_hindi(jsonObj.getString("cam_desc_in_hindi"));//
                                    todayTask.setAppSdesc_hindi(jsonObj.getString("cam_short_desc_in_hindi"));
                                    todayTask.setWebUrl_hindi(jsonObj.getString("webUrl_in_hindi"));//
                                    todayTask.setPause(jsonObj.getString("pous"));//               used in capping lim
                                    todayTask.setWorkStatus(jsonObj.getString("assing_work_status"));//
                                    todayTask.setClickBLabel(jsonObj.getString("button_text"));
                                    todayTask.setIcon(jsonObj.getString("icone_url"));


                                    totalCount=totalCount+Double.parseDouble(jsonObj.getString("pkgcost"));

                                    double countToDecimal = Math.floor(totalCount * 100) / 100;
                                    Home.today_task_point.setText(String.format(Locale.getDefault(),"%.2f", countToDecimal));


                                    if (!jsonObj.getString("verifynew_user").equalsIgnoreCase("Y"))
                                    {
                                        list.add(todayTask);
                                    }

                                }

                            }

                            HomeRecyclerAdapter_TodayTasks globalAdapter= new HomeRecyclerAdapter_TodayTasks(activity,list);
                            today_task_homeRecycler.setAdapter(globalAdapter);
                            today_task_homeRecycler.setLayoutManager(new GridLayoutManager(activity, 2));
                            today_task_homeRecycler.setHasFixedSize(true);
                            globalAdapter.notifyDataSetChanged();
                            int gidSize=list.size()/2+list.size()%2;
                            try {
                                today_task_homeRecycler.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dpToPx(155)*gidSize));

                            }catch (Exception e){
                                today_task_homeRecycler.setNestedScrollingEnabled(true);
                                today_task_homeRecycler.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            }

                        } catch (JSONException e) {
                            Log.e("frag_tda error",e.toString());
                            e.printStackTrace();

                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mSwipeRefreshLayout.setRefreshing(false);

                Toast.makeText(activity, "Server is not Responding !!", Toast.LENGTH_SHORT).show();

                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_CUSTMRID, _userId);
                params.put(KEY_CLIENT_ID, clientId(activity));
                Log.e("Frag_todayTsk parms",params.toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(activity));
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    ///////////////////load viral news//////////////////////
    private void loadViralNews(){


         myPrefsNews= Objects.requireNonNull(activity).getSharedPreferences("newsFilter", Context.MODE_PRIVATE);
         String filter=myPrefsNews.getString("category","google-news-in");
//        /////////////id for created wte.jrappdeveloper@gmail.com////////////////////////////////////
        String news_API="https://newsapi.org/v2/top-headlines?sources=" +
                filter +
                "&apiKey=28e9c0851efd456abf782dc9a37e33e2";

//        Log.e("url",news_API);


//        String news_API2="https://newsapi.org/v2/top-headlines?sources=medical-news-today&apiKey=28e9c0851efd456abf782dc9a37e33e2";
//        String news_API3="https://newsapi.org/v2/top-headlines?sources=espn-cric-info&apiKey=28e9c0851efd456abf782dc9a37e33e2";
//        String entertain="https://newsapi.org/v2/top-headlines?sources=buzzfeed&apiKey=28e9c0851efd456abf782dc9a37e33e2";





        StringRequest stringRequest = new StringRequest(Request.Method.GET, news_API,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        ArrayList<ViralNewsModel> virallist = new ArrayList<>();


                        try {
                            JSONObject jsonObject = new JSONObject(response);
//                            Log.e("news",response);
                            if (jsonObject.get("status").equals("ok")){

                                JSONArray article=jsonObject.getJSONArray("articles");

                                for (int i=0;i<article.length();i++){

                                    ViralNewsModel viralNewsModel=new ViralNewsModel();

                                    viralNewsModel.setAuthor(article.getJSONObject(i).getString("author"));
                                    viralNewsModel.setTitle(article.getJSONObject(i).getString("title"));
                                    viralNewsModel.setDescription(article.getJSONObject(i).getString("description"));
                                    viralNewsModel.setUrl(article.getJSONObject(i).getString("url"));
                                    viralNewsModel.setUrlToImage(article.getJSONObject(i).getString("urlToImage"));
                                    viralNewsModel.setPublishedAt(article.getJSONObject(i).getString("publishedAt"));

                                    virallist.add(viralNewsModel);

                                }
                            }

//////////////////////////////////// Setup and Handover data to recyclerview

                            HomeRecyclerAdapter_viralNews globalAdapter= new HomeRecyclerAdapter_viralNews(activity,virallist);



                            viral_news_homeRecycler.setAdapter(globalAdapter);
                            viral_news_homeRecycler.setLayoutManager(new LinearLayoutManager(activity));
                            viral_news_homeRecycler.setHasFixedSize(true);

                            try{
                                viral_news_homeRecycler.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dpToPx(309)*virallist.size()));

                            }catch (Exception e){
                                viral_news_homeRecycler.setNestedScrollingEnabled(true);
                                viral_news_homeRecycler.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            }
//                            viral_news_homeRecycler.expandFraomCentre();
                            if (focus==1){
                                Log.e("true","true");
                                viral_news_homeRecycler.setFocusable(true);
                                viral_news_homeRecycler.setFocusableInTouchMode(true);
                                today_task_homeRecycler.clearFocus();

                                viral_news_homeRecycler.requestFocus();
                                scroll.scrollTo(0, viral_news_homeRecycler.getBottom());
                            }

                        } catch (JSONException e) {
                            Log.e("error_news catch",e.toString());

                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error_news",error.toString());


                newscard.setVisibility(View.GONE);
//                Toast.makeText(activity, "Server is not Responding !!", Toast.LENGTH_SHORT).show();
                Log.e("err newa", error.toString());

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(activity));
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


    }



/**////////////////////////////////////shaking trophy////////////////////////////

    public void shakeImage() {
/**/        Animation shake;

            try{
                shake = AnimationUtils.loadAnimation(activity, R.anim.moving_trophy);
                trophy.startAnimation(shake);
            }catch (Exception e){
                e.printStackTrace();
            }
/**/
/**/
/**/
/**/
///**/
///**/        Handler handler=new Handler();
///**/        handler.postDelayed(new Runnable() {
///**/            @Override
///**/            public void run() {
///**/
// /**/           }
// /**/       },500);
/**/    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
           try {
               shakeImage(); //this function can change value of mInterval.
           } finally {
               int mInterval = 1500;
               mHandler.postDelayed(mStatusChecker, mInterval);
           }
       }
    };

   void startRepeatingTask() {
/**/        mStatusChecker.run();
/**/    }



    @Override
    public void onPause() {
        super.onPause();
        progressDialog.dismiss();
    }








    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public int dpToPx(int dp) {
        float density = Objects.requireNonNull(activity).getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }


}
