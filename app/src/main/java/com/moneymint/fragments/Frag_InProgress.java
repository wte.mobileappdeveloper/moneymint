package com.moneymint.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneymint.R;
import com.moneymint.configs.Configs;
import com.moneymint.customViews.MyRecyclerView;
import com.moneymint.models.TodayTaskModel;
import com.moneymint.recyclerAdapter.HomeRecyclerAdapter_InProgress;
import com.moneymint.utils.InternetStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.moneymint.configs.Configs.KEY_CLIENT_ID;
import static com.moneymint.configs.Configs.clientId;

public class Frag_InProgress extends Fragment {
    MyRecyclerView homeRecycler;
    public static final String KEY_USER_ID = "customer_id";
    public String _userId;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ImageView img_no_task_progressing;
    private TextView txt_no_task_progressing;
    public View view;
    public LinearLayout linearLayout;
    ArrayList<TodayTaskModel> list = new ArrayList<>();
    private ProgressDialog progressDialog;
    Activity activity;


    public Frag_InProgress() {
    }

    @SuppressLint("ValidFragment")
    public Frag_InProgress(Activity activity) {
        this.activity=activity;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view= inflater.inflate(R.layout.fragment_in_progress, container, false);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");


//        View view= inflater.inflate(R.layout.fragment_today_task, container, false);
        homeRecycler=view.findViewById(R.id.home_recycler);
        linearLayout=view.findViewById(R.id.linear_layout);
        img_no_task_progressing =  view.findViewById(R.id.img_no_task_progressing);
        txt_no_task_progressing =  view.findViewById(R.id.txt_no_task_progressing);
        mSwipeRefreshLayout =  view.findViewById(R.id.swifeRefresh_for_inprogress);
        mSwipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.MAGENTA);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                checkConnection();
            }
        });

        SharedPreferences preferences = this.activity.getSharedPreferences("login", Context.MODE_PRIVATE);
        _userId = preferences.getString("User_Id", "");
        fetchInProgress();
        return view;


    }


    private void checkConnection() {
        String message;
        Snackbar snackbar;
        int color;
        if (InternetStatus.getInstance(activity).isOnline()) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;
            fetchInProgress();
        } else {
            message = "Sorry Check Your Internet Connection !!";
            txt_no_task_progressing.setText(message);
            color = Color.RED;
            mSwipeRefreshLayout.setRefreshing(false);
        }
        snackbar = Snackbar.make(linearLayout, message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView textView =  sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }

    private void fetchInProgress() {
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_inProgress,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Frag_inprogress resp",response);
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);


                            if (jsonObject.optString("error").equalsIgnoreCase("true")) {

                                txt_no_task_progressing.setVisibility(View.VISIBLE);
                                list.clear();

//                                txt_no_task_progressing.setText(jsonObject.optString("No Task In Progess"));
                                img_no_task_progressing.setVisibility(View.VISIBLE);

                            } else if (jsonObject.optString("error").equalsIgnoreCase("false")) {
                                txt_no_task_progressing.setVisibility(View.INVISIBLE);
                                img_no_task_progressing.setVisibility(View.INVISIBLE);
                                list.clear();
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    TodayTaskModel todayTask = new TodayTaskModel();
                                    JSONObject jsonObj = jsonArray.getJSONObject(i);

//                                    String package_ID = jsonObj.getString("campainid");
//                                    String workassign_date = jsonObj.getString("add_dt");
//                                    String workStatus = jsonObj.getString("assing_work_status");
//                                    String package_Name = jsonObj.getString("campainname");
//                                    String Amount_Credit = jsonObj.getString("user_cast");
//                                    String work_url = jsonObj.getString("work_url");
//                                    String webView_url = jsonObj.getString("webUrl");
//                                    String appsdesc = jsonObj.getString("com_desc");
//                                    String appdesc = jsonObj.getString("com_desc");
//                                    String icon = jsonObj.getString("Icon");
//                                    String taskDuration=jsonObj.getString("task_duration");
//// set Api values

// For Set data
                                    todayTask.setAppDesc_hindi(jsonObj.getString("cam_desc_in_hindi"));//
                                    todayTask.setAppSdesc_hindi(jsonObj.getString("cam_short_desc_in_hindi"));
                                    todayTask.setWebUrl_hindi(jsonObj.getString("webUrl_in_hindi"));//

                                    todayTask.setAmount_Credit(jsonObj.getString("pkgcost"));//
                                    todayTask.setPackage_id(jsonObj.getString("campaign_id"));//
                                    todayTask.setPackage_Name(jsonObj.getString("camping_name"));//
//                                        todayTask.setWorkassign_Date(jsonObj.getString("assign_work_date"));//
                                    todayTask.setWebUrl(jsonObj.getString("webUrl"));//
                                    todayTask.setWork_url(jsonObj.getString("com_url"));//
                                    todayTask.setAppDesc(jsonObj.getString("com_desc"));//
                                    todayTask.setWorkStatus(jsonObj.getString("assing_work_status"));//
                                    todayTask.setClickBLabel("Complete");
                                    todayTask.setIcon(jsonObj.getString("icone_url"));
                                    todayTask.setAppSdesc(jsonObj.getString("com_sdesc"));
                                    todayTask.setPause(jsonObj.getString("pous"));//



                                        list.add(todayTask);

                                }

                            }
                            HomeRecyclerAdapter_InProgress globalAdapter= new HomeRecyclerAdapter_InProgress(activity,list);
                            homeRecycler.setAdapter(globalAdapter);
                            homeRecycler.setLayoutManager(new LinearLayoutManager(activity));
                            homeRecycler.setHasFixedSize(true);
                            homeRecycler.expandFraomCentre();

                            mSwipeRefreshLayout.setRefreshing(false);

                        } catch (JSONException e) {

                            Log.e("error in progress",e.toString());
                            e.printStackTrace();
                        }

                        mSwipeRefreshLayout.setRefreshing(false);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(activity, "Server is Not Responding !!", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_CLIENT_ID, clientId(activity));
                params.put(KEY_USER_ID, _userId);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

}
