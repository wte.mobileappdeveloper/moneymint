package com.moneymint.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moneymint.R;
import com.moneymint.models.ListNotifyModel;
import com.moneymint.recyclerAdapter.NotificationAdapter;
import com.moneymint.utils.notifications.DatabaseNotification;

import java.util.ArrayList;
import java.util.Collections;


public class NotificationFragment extends Fragment {
    private Context context;
    public RecyclerView listView;

    ArrayList<ListNotifyModel> list=new ArrayList<>();
    public static Cursor c;

    static public ArrayList<String> notificationDate,notificationBody;
    public static DatabaseNotification databaseOperations;

    public NotificationFragment() {
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_notification, container, false);

        databaseOperations=new DatabaseNotification(getContext());
        listView = view.findViewById(R.id.dialoglist);
        notificationBody=new ArrayList<>();
        notificationDate=new ArrayList<>();




        list.clear();
        getNotification();



        return view;
    }



    public void getNotification(){
        notificationBody.clear();
        notificationDate.clear();
        c=databaseOperations.selectAllData();

        if(c!=null)
        {
            c.moveToFirst();
            Log.e("sql",String.valueOf(c.isBeforeFirst()));
            if(!c.isBeforeFirst()) {
                do {
                    Log.e("sql1",String.valueOf(c.getString(0)));
                    notificationBody.add(c.getString(0));
                    notificationDate.add(c.getString(1));
                } while (c.moveToNext());
            }

            Collections.reverse(notificationBody);
            Collections.reverse(notificationDate);


            for (int i=0;i<notificationBody.size();i++){
                ListNotifyModel listNotifyModel = new ListNotifyModel();
                listNotifyModel.setNotifyMsg(notificationBody.get(i));
                listNotifyModel.setNotifyDate(notificationDate.get(i));
                list.add(listNotifyModel);
                Log.e("sql2",String.valueOf(notificationBody.get(i)));

                NotificationAdapter notificationAdapter=new NotificationAdapter(getContext(),list);
                listView.setAdapter(notificationAdapter);
                listView.setLayoutManager(new LinearLayoutManager(getContext()));
                listView.setHasFixedSize(true);


            }



        }
    }

}
