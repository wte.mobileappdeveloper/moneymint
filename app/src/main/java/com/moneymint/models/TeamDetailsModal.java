package com.moneymint.models;

public class TeamDetailsModal {

    public String getMobile() {
        return mobile;
    }

    private String mobile;
    private String no_of_user,level,name,email,referral_amount;

    public String getNo_of_user() {
        return no_of_user;
    }

    public String getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getReferral_amount() {
        return referral_amount;
    }

    public TeamDetailsModal(String name, String email, String referral_amount,String mobile) {

        this.name = name;
        this.email = email;
        this.referral_amount = referral_amount;
        this.mobile=mobile;
    }

    public  TeamDetailsModal(String level, String no_of_user) {

        this.no_of_user = no_of_user;
        this.level = level;
    }
}
