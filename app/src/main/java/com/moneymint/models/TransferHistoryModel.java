package com.moneymint.models;

/**
 * Created by android on 8/3/18.
 */

public class TransferHistoryModel {

    String entrydate;
    String paid_amt;
    String balance;
    String tds;

    public String getTds() {
        return tds;
    }

    public void setTds(String tds) {
        this.tds = tds;
    }

    public String getAdmnChrge() {
        return admnChrge;
    }

    public void setAdmnChrge(String admnChrge) {
        this.admnChrge = admnChrge;
    }

    String admnChrge;

    public String getEntrydate() {
        return entrydate;
    }

    public void setEntrydate(String entrydate) {
        this.entrydate = entrydate;
    }

    public String getPaid_amt() {
        return paid_amt;
    }

    public void setPaid_amt(String paid_amt) {
        this.paid_amt = paid_amt;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
