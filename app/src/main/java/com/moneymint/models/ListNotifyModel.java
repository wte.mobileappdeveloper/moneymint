package com.moneymint.models;

/**
 * Created by Android on 7/17/2017.
 */

public class ListNotifyModel {
    public String getNotifyMsg() {
        return notifyMsg;
    }

    public ListNotifyModel() {

    }

    public void setNotifyMsg(String notifyMsg) {
        this.notifyMsg = notifyMsg;
    }

    public String getNotifyDate() {
        return notifyDate;
    }

    public void setNotifyDate(String notifyDate) {
        this.notifyDate = notifyDate;
    }

    private String notifyMsg;
    private String notifyDate;
}
