package com.moneymint.models;

/**
 * Created by android on 3/1/18.
 */

public class UserDetailsModel {
    public String _UName,_EmailID,_Mobileno,_City,_AccountNo,_BankName,_BranchAddress,_IFSCCode;

    public String get_UName() {
        return _UName;
    }

    public void set_UName(String _UName) {
        this._UName = _UName;
    }

    public String get_EmailID() {
        return _EmailID;
    }

    public void set_EmailID(String _EmailID) {
        this._EmailID = _EmailID;
    }

    public String get_Mobileno() {
        return _Mobileno;
    }

    public void set_Mobileno(String _Mobileno) {
        this._Mobileno = _Mobileno;
    }

    public String get_City() {
        return _City;
    }

    public void set_City(String _City) {
        this._City = _City;
    }

    public String get_AccountNo() {
        return _AccountNo;
    }

    public void set_AccountNo(String _AccountNo) {
        this._AccountNo = _AccountNo;
    }

    public String get_BankName() {
        return _BankName;
    }

    public void set_BankName(String _BankName) {
        this._BankName = _BankName;
    }

    public String get_BranchAddress() {
        return _BranchAddress;
    }

    public void set_BranchAddress(String _BranchAddress) {
        this._BranchAddress = _BranchAddress;
    }

    public String get_IFSCCode() {
        return _IFSCCode;
    }

    public void set_IFSCCode(String _IFSCCode) {
        this._IFSCCode = _IFSCCode;
    }
}
