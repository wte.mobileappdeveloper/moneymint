package com.moneymint.models;

/**
 * Created by lalji on 18/11/17.
 */

public class StateModel {

    String stateID;
    String stateNAme;

    public String getStateID() {
        return stateID;
    }

    public void setStateID(String stateID) {
        this.stateID = stateID;
    }

    public String getStateNAme() {
        return stateNAme;
    }

    public void setStateNAme(String stateNAme) {
        this.stateNAme = stateNAme;
    }



}
