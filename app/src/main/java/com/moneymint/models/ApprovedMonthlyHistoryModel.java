package com.moneymint.models;

/**
 * Created by android on 8/3/18.
 */

public class ApprovedMonthlyHistoryModel {


    String selfincome,incomebychild,MonthYear,monthwisetotal;

    public String getSelfincome() {
        return selfincome;
    }

    public void setSelfincome(String selfincome) {
        this.selfincome = selfincome;
    }

    public String getIncomebychild() {
        return incomebychild;
    }

    public void setIncomebychild(String incomebychild) {
        this.incomebychild = incomebychild;
    }

    public String getMonthYear() {
        return MonthYear;
    }

    public void setMonthYear(String monthYear) {
        MonthYear = monthYear;
    }

    public String getMonthwisetotal() {
        return monthwisetotal;
    }

    public void setMonthwisetotal(String monthwisetotal) {
        this.monthwisetotal = monthwisetotal;
    }
}
