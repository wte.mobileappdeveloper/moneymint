package com.moneymint.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneymint.R;
import com.moneymint.configs.Configs;
import com.moneymint.utils.InternetConnectivity.BaseActivity;
import com.moneymint.CommonFunction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.android.volley.Response.ErrorListener;
import static com.android.volley.Response.Listener;
import static com.moneymint.configs.Configs.KEY_CLIENT_ID;
import static com.moneymint.configs.Configs.clientId;

public class SignUp extends BaseActivity {
    String[] gender = {"Male", "Female"};
    int state;
    String parMobile="1111111111";
    Spinner sp_gender, sp_state;
    ProgressDialog progressDialog;
    ProgressBar refer_progressBar;
    ProgressBar mobileProgress;
    Dialog dialog;
    HashMap<String, Integer> statelist=new HashMap<>();


    // Declare Views
    EditText edt_uName, edt_eMail, edt_mobile, edt_pwd, edt_city, txt_CUpwd,txt_Reffer;
    Button btn_register;
    String refferCode;
    TextView txt_current_date;


    public static final String KEY_USER_NAME = "name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_PASSWORD = "password";
    String current_date;
    public static final String KEY_GENDER = "gender";
    public static final String KEY_CITY = "city";
    public static final String KEY_DOB = "dob";
    public static final String KEY_STATE = "state";
    private static final String KEY_REFER_MOBILE="parent_mobile";
    boolean remeberDetails;
    ImageView img_mobile_done,img_mobile_refer;
    SharedPreferences saveRefPref;

    int flag=0;  ////used to check correct referral




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Log.e("crte","jj");

        Toolbar toolbar =  findViewById(R.id.toggle_toolbar);
        toolbar.setTitle("Registration");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getCode();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        // Check registration details exits or not
        remeberDetails= getSharedPreferences("login", Context.MODE_PRIVATE).getBoolean("FLAG",false);

        if (remeberDetails)
        {
            Intent i = new Intent(SignUp.this,VarificationOTP.class);
            startActivity(i);
            finish();
        }


        txt_current_date =  findViewById(R.id.txt_current_date_spam);
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
//        txt_current_date.setText(date);
        sp_gender = findViewById(R.id.sp_gender);
        sp_state = findViewById(R.id.sp_state);
        // This Adapter For Gender
        ArrayAdapter<String> ap_gender = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, gender);
        ap_gender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_gender.setAdapter(ap_gender);

        findViewID();

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);

        // Calling viewfindById()
        saveRefPref=getSharedPreferences("REFER_ID", Context.MODE_PRIVATE);

        btn_register=findViewById(R.id.btn_register);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog.setMessage("Registering.....");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                // This function for validation
                validation();
            }
        });


        findViewById(R.id.doblayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int cYear, cMonth, cDay;
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                cYear = c.get(Calendar.YEAR);
                cMonth = c.get(Calendar.MONTH);
                cDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(SignUp.this,R.style.DialogTheme,new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        DecimalFormat formatter = new DecimalFormat("00");
                        String month = formatter.format(monthOfYear + 1);
                        String day = formatter.format(dayOfMonth);

                        current_date = year + "-" + month + "-" + day;
                        txt_current_date.setText(current_date);


                    }
                }, cYear, cMonth, cDay);

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

            }
        });
        statelist();
    }

    @Override
    public void onStart() {
        super.onStart();



    }
    // For mapping ID

    private void findViewID() {

        edt_uName = findViewById(R.id.txt_Uuser_Name);
        edt_eMail = findViewById(R.id.txt_Uemail);
        edt_mobile = findViewById(R.id.txt_Umobile);
        edt_pwd = findViewById(R.id.txt_Upwd);
        edt_city = findViewById(R.id.txt_reg_City);
        txt_CUpwd=findViewById(R.id.txt_UCnfrm_pwd);
        txt_Reffer=findViewById(R.id.txt_Reffer);
        edt_mobile.addTextChangedListener(watch);
        img_mobile_done=findViewById(R.id.img_mobile_done);
        img_mobile_refer=findViewById(R.id.img_mobile_refer);
        txt_CUpwd.addTextChangedListener(cnfrmPassword);
        txt_Reffer.addTextChangedListener(watchRefer);
        refer_progressBar=findViewById(R.id.reffer_progress);
        mobileProgress=findViewById(R.id.mobile_progress);


        try {
            refferCode=getSharedPreferences("REFER_ID", Context.MODE_PRIVATE).getString("REF_USERID","0000000000");
            Log.e("signup parent id",refferCode);

            if (refferCode.equals("0000000000"))
            {
                txt_Reffer.setText("");
            }else {
                txt_Reffer.setEnabled(false);
                txt_Reffer.setText(refferCode);
            }
        }catch (NullPointerException e)
        {
            Log.e("signup parent id error",e.toString());
            e.printStackTrace();

        }




    }

    //.......................Set Text Watcher For Check registered number
    TextWatcher watch = new TextWatcher(){

        @Override
        public void afterTextChanged(Editable arg0) {}

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {


//             output.setText(s);
            if(a ==9){
                if (s.length()==10)
                {
                    mobileNumberAvilable(s.toString());
                    img_mobile_done.setVisibility(View.GONE);
                }
                img_mobile_done.setVisibility(View.GONE);

            }else
            {
                img_mobile_done.setVisibility(View.GONE);
            }
        }};

    //.......................Set Text Watcher For Check Referal number
    TextWatcher watchRefer = new TextWatcher(){

        @Override
        public void afterTextChanged(Editable arg0) {}

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
//             if(a ==9){
            if (s.length()==10)
            {
                referMobileNumberCheck(s.toString());
                img_mobile_refer.setVisibility(View.GONE);
            }

            else if(s.length()>0&&s.length()<10){
                flag=0;
                img_mobile_refer.setVisibility(View.GONE);

            }else
            {
                img_mobile_refer.setVisibility(View.GONE);
            }
        }
    };



    //.......................Set Text Watcher For Check confirm password

    TextWatcher cnfrmPassword=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (s.length()==edt_pwd.getText().length()&&!txt_CUpwd.getText().toString().equals(edt_pwd.getText().toString())){
                txt_CUpwd.setError("Password doesn't match");
                txt_CUpwd.requestFocus();
            }else {

            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };



    // For Validation
    private void validation() {

        if (edt_uName.getText().toString().isEmpty()  || edt_uName.getText().length() == 0) {
            edt_uName.setError("Enter User Name");
            edt_uName.requestFocus();
        } else if (edt_eMail.getText().toString().isEmpty() || edt_eMail.getText().toString().length() == 0||!CommonFunction.isValideEmailPattern(edt_eMail.getText().toString())) {
            edt_eMail.setError("Please Enter Correct Email");
            edt_eMail.requestFocus();
        } else if (edt_mobile.getText().toString().isEmpty() || edt_mobile.getText().length() <10 ) {
            edt_mobile.setError("Enter valid mobile number");
            edt_mobile.requestFocus();
        } else if (txt_current_date.getText().toString().isEmpty()|| txt_current_date.getText().toString().equalsIgnoreCase("D.O.B.")) {
            Toast.makeText(this, "Enter Date Of Birth", Toast.LENGTH_SHORT).show();

        } else if (edt_pwd.getText().toString().isEmpty() || edt_pwd.getText().toString().length() < 4) {

            edt_pwd.setError("Enter Valid password(Minimum 4 characters)");
            edt_pwd.requestFocus();

        }else if (!txt_CUpwd.getText().toString().equals(edt_pwd.getText().toString())||txt_CUpwd.getText().toString().length()==0)
        {
            txt_CUpwd.setError("Password doesn't match");
            txt_CUpwd.requestFocus();
        }
        else if(edt_city.getText().toString().isEmpty() || edt_city.getText().toString().length() == 0)
        {
            edt_city.setError("Enter city name");
            edt_city.requestFocus();

        }else if(sp_gender.getSelectedItem().toString().isEmpty()||sp_gender.getSelectedItem().toString().length()==0) {
            Toast.makeText(this, "Select Gender", Toast.LENGTH_SHORT).show();
            sp_gender.requestFocus();
        }else if (sp_state.getSelectedItem().toString().equalsIgnoreCase("SELECT STATE")||sp_state.getSelectedItem().toString().length()==0) {
            Toast.makeText(this, "Select State", Toast.LENGTH_SHORT).show();
            sp_state.requestFocus();

        }else if(txt_Reffer.getText().toString().isEmpty() || txt_Reffer.getText().toString().length() == 0){
//            confirmDialog(SignUp.this);


//            -------------mandatory referral code--------------
            txt_Reffer.setError("Enter Referral Mobile Number");
            txt_Reffer.requestFocus();

        }
        else if (edt_mobile.getText().equals(txt_Reffer.getText())){
            edt_mobile.setError("Mobile and referral mobile can't be same");
        }
        else if (refer_progressBar.getVisibility()==View.VISIBLE){
            Toast.makeText(SignUp.this,"Please wait while we check referral code",Toast.LENGTH_SHORT).show();
        }else if (mobileProgress.getVisibility()==View.VISIBLE){
            Toast.makeText(SignUp.this,"Please wait while we validate mobile number",Toast.LENGTH_SHORT).show();
        }else if (flag==0){
            Toast.makeText(SignUp.this,"Referral code is not valid",Toast.LENGTH_SHORT).show();
        }
        else
        {

            // save all registration details function
            saveRegistrationDetails();

        }

    }

    private void saveRegistrationDetails() {
        state=statelist.get(sp_state.getSelectedItem().toString());

        SharedPreferences regisPreference= getSharedPreferences("REGIST_DETAILS",MODE_PRIVATE);

        SharedPreferences.Editor editor= regisPreference.edit();
        editor.putString("UNAME",edt_uName.getText().toString());
        editor.putString("EMAIL",edt_eMail.getText().toString());
        editor.putString("MOBILE",edt_mobile.getText().toString());
        editor.putString("PWD",edt_pwd.getText().toString());
        editor.putString("GENDER",sp_gender.getSelectedItem().toString());
        editor.putString("CITY",edt_city.getText().toString());
        editor.putInt("STATE",state);
        editor.putString("REFFER_CODE",txt_Reffer.getText().toString());
        editor.putString("DOB",txt_current_date.getText().toString());
        editor.apply();
//        Toast.makeText(SignUp.this,"Registration Record Saved",Toast.LENGTH_SHORT).show();

//        startActivity(new Intent(SignUp.this,VarificationOTP.class));

        registrationParse();

    }

    public String loadJSONFromAsset() {
        String json ;
        try {
            InputStream is = getApplicationContext().getAssets().open("country_code.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    private void getCode(){
        try {
            Spinner countrycode=findViewById(R.id.countrySpinner);
            String country;
            String code;
            JSONArray obj = new JSONArray(loadJSONFromAsset());
            ArrayList cc=new ArrayList();
            final ArrayList cde=new ArrayList();

            for (int i = 0; i < obj.length(); i++) {

                JSONObject jsonObject=obj.getJSONObject(i);
                country=jsonObject.getString("name");
                code=jsonObject.getString("dial_code");
                cc.add(country+"("+code+")");
                cde.add(code);

            }

            countrycode.setAdapter(new ArrayAdapter<String>(SignUp.this,
                    android.R.layout.simple_spinner_dropdown_item,
                    cc));

            countrycode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    ((TextView) view).setText(cde.get(position).toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void mobileNumberAvilable(String _mobile) {

        mobileProgress.setVisibility(View.VISIBLE);
        final String mobile = _mobile;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.chekcUser,
                new Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("number check resp",response);

                        try {
                            JSONObject jObject = new JSONObject(response);

                            if (jObject.optString("status").equalsIgnoreCase("Y") ) {

                                edt_mobile.setError("Number already exists !");
                                edt_mobile.requestFocus();
                                img_mobile_done.setVisibility(View.GONE);
                                mobileProgress.setVisibility(View.INVISIBLE);

                            }else
                            {
                                img_mobile_done.setVisibility(View.VISIBLE);
                                mobileProgress.setVisibility(View.INVISIBLE);


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("Server Side Erorr",error.toString());

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_MOBILE, mobile);
                params.put(KEY_CLIENT_ID,Configs.clientId(SignUp.this));
                Log.e("number check params",params.toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


    }

    public void referMobileNumberCheck(final String refer_mobile) {
        refer_progressBar.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.chekcUser,
                new Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Signuprefer response",response);

                        try {
                            JSONObject jObject = new JSONObject(response);

                            if (jObject.optString("status").equalsIgnoreCase("Y") ) {
                                txt_Reffer.requestFocus();
                                img_mobile_refer.setVisibility(View.VISIBLE);
                                refer_progressBar.setVisibility(View.GONE);

//                                referrarName(jObject.getString("Name"));
                                flag=1;





                            }else
                            {
                                txt_Reffer.setError("Wrong referral code !");
                                img_mobile_refer.setVisibility(View.GONE);
                                refer_progressBar.setVisibility(View.GONE);
                                flag=0;

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("Server Side Erorr",error.toString());

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_MOBILE, refer_mobile);
                params.put(KEY_CLIENT_ID,Configs.clientId(SignUp.this));
                Log.e("Signuprefer params",parMobile.toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }



    public void confirmDialog(final Activity activity)  {

        final Dialog dialog = new Dialog(activity, R.style.floatingDialog);
        dialog.setContentView(R.layout.dialog_confirm);
        dialog.setCancelable(false);
        TextView proceed =  dialog.findViewById(R.id.txt_proceed);
        proceed.setText("Yes");
        TextView message =  dialog.findViewById(R.id.txt_exit);
        TextView recheck =  dialog.findViewById(R.id.txt_recheck);
        recheck.setText("No");
        message.setText("Are you sure to continue without referral code?");
        recheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveRegistrationDetails();
                dialog.dismiss();

            }
        });
        dialog.show();

    }


    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }


    @Override
    public void onBackPressed() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        super.onBackPressed();
        getSharedPreferences("REGIST_DETAILS",MODE_PRIVATE).edit().clear().apply();
    }

    @Override
    protected void onResume() {
        super.onResume();



//        SharedPreferences regisPreference= getSharedPreferences("REGIST_DETAILS",MODE_PRIVATE);
//
//
//        if (regisPreference.getBoolean("FLAG",false)){
//
//            edt_uName.setText(regisPreference.getString("UNAME",""));
//            edt_eMail.setText(regisPreference.getString("EMAIL",""));
//            edt_mobile.setText(regisPreference.getString("MOBILE",""));
//            edt_pwd.setText(regisPreference.getString("PWD",""));
//            edt_city.setText(regisPreference.getString("CITY",""));
//            txt_CUpwd.setText(regisPreference.getString("PWD",""));
//            txt_Reffer.setText(regisPreference.getString("REFFER_CODE",""));
//            sp_state.setSelection(regisPreference.getInt("STATE",0));


//        }


    }


    private void referrarName(String name){

//        flag=1;

        dialog=new Dialog(SignUp.this);


        dialog = new Dialog(SignUp.this,R.style.floatingDialog);
        dialog.setContentView(R.layout.dialog_confirm);
        TextView message=dialog.findViewById(R.id.txt_exit);
        message.setText("You are referred by\n"+name);
        TextView proceed =  dialog.findViewById(R.id.txt_proceed);
        TextView recheck =  dialog.findViewById(R.id.txt_recheck);

        recheck.setVisibility(View.GONE);
        proceed.setText("Confirm");
        recheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
        dialog.show();

    }



    private void registrationParse() {

        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_signup, new Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
                Log.e("response regestration",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("Regis Response",response);
                    if (jsonObject.optString("error").toString().equalsIgnoreCase("false")) {



                        SharedPreferences myPrefsLogin = getSharedPreferences("login", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = myPrefsLogin.edit();
                        editor.putString("User_Id", jsonObject.getString("customer_id"));
                        editor.putBoolean("FLAG",true);
                        editor.apply();
                        startActivity(new Intent(SignUp.this,VarificationOTP.class));
                        finish();
                    } else {
                        Toast.makeText(SignUp.this, "Mobile or Email already registered", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }, new ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SignUp.this, "Some error occurred", Toast.LENGTH_SHORT).show();
                if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put(KEY_USER_NAME, edt_uName.getText().toString());
                params.put(KEY_EMAIL, edt_eMail.getText().toString());
                params.put(KEY_MOBILE, edt_mobile.getText().toString());
                params.put(KEY_PASSWORD, edt_pwd.getText().toString());
                params.put(KEY_GENDER, sp_gender.getSelectedItem().toString().substring(0,1));
                params.put(KEY_CITY, edt_city.getText().toString());

                params.put(KEY_DOB, txt_current_date.getText().toString());

                params.put(KEY_STATE, String.valueOf(state));
                params.put(KEY_REFER_MOBILE,txt_Reffer.getText().toString());
                params.put(KEY_CLIENT_ID, clientId(SignUp.this));
                Log.e("Regis",params.toString());
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


    private void statelist() {

        progressDialog.setMessage("Loading..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_stateList, new Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
                Log.e("response regestration",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("Regis Response",response);
                    if (jsonObject.optString("error").equalsIgnoreCase("false")) {


                        JSONArray state=jsonObject.getJSONArray("statelist");


                        statelist.clear();
//                        statelist.put("SELECT STATE",0);
                        for (int i=0;i<state.length();i++){

                            JSONObject stt=state.getJSONObject(i);

                            statelist.put(stt.getString("state"),Integer.parseInt(stt.getString("id")));

                        }


                        ArrayList<String> statename = new ArrayList<String>(statelist.keySet());
                        Collections.sort(statename, String.CASE_INSENSITIVE_ORDER);
                        ArrayList<String> statearraylist=new ArrayList<>();
                        statearraylist.add("SELECT STATE");
                        statearraylist.addAll(statename);


                        ArrayAdapter<String> adapter =new ArrayAdapter<String>(SignUp.this,android.R.layout.simple_spinner_item, statearraylist);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sp_state.setAdapter(adapter);



                    } else {
                        Toast.makeText(SignUp.this, "Error fetching State List", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }

            }
        }, new ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(SignUp.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
                startActivity(new Intent(SignUp.this,Login.class));
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put(KEY_USER_NAME, edt_uName.getText().toString());
                params.put(KEY_EMAIL, edt_eMail.getText().toString());
                params.put(KEY_MOBILE, edt_mobile.getText().toString());
                params.put(KEY_PASSWORD, edt_pwd.getText().toString());
                params.put(KEY_GENDER, sp_gender.getSelectedItem().toString().substring(0,1));
                params.put(KEY_CITY, edt_city.getText().toString());

                params.put(KEY_DOB, txt_current_date.getText().toString());

                params.put(KEY_STATE, String.valueOf(state));
                params.put(KEY_REFER_MOBILE,txt_Reffer.getText().toString());
                params.put(KEY_CLIENT_ID, clientId(SignUp.this));
                Log.e("Regis",params.toString());
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }



    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }        super.onDestroy();
    }


}




