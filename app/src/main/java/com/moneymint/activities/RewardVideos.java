package com.moneymint.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.moneymint.ApiHandler.VolleySingleton;
import com.moneymint.CommonFunction;
import com.moneymint.LocalSaved.PreferenceHelper;
import com.moneymint.R;
import com.moneymint.configs.Configs;
import com.moneymint.utils.InternetConnectivity.BaseActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.moneymint.CommonFunction.getCurrentDate;
import static com.moneymint.CommonFunction.md5;
import static com.moneymint.configs.Configs.KEY_CLIENT_ID;
import static com.moneymint.configs.Configs.clientId;

import static com.moneymint.utils.Constant.COUNT_DOWN;
import static com.moneymint.utils.Constant.REWARDSESSION;
import static com.moneymint.utils.Constant.SESSION_DATE;
import static com.moneymint.utils.Constant.USER_CAST;
import static com.moneymint.utils.Constant.WEB_THE_DIRT_SHEET;

public class RewardVideos extends BaseActivity{
    public static final String KEY_USER_ID = "customer_id";
    private Toolbar toolbar;
    private ImageView imgRightArrow,imgLeftArrow;
    public  CountDownTimer countDownTimer;
    private TextView txtCountDown,txtOutOFDown,txtCountCircleDown;
    Integer i=0;
    private RelativeLayout lytCount;
    private WebView webTopNews;
    boolean isVideoCompleted=true;
    boolean isCounterFinished=true;
    boolean isReplay=true;
    ProgressDialog progressDoalog;
    String _userId;
    String deviceId;
    private ProgressBar webLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward_videos);

        toolbar = findViewById(R.id.toggle_toolbar);
        TextView toolactName = findViewById(R.id.act_name);

        toolactName.setText("Reward Videos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgRightArrow = findViewById(R.id.imgRightArrow);
        txtCountDown = findViewById(R.id.txtCountDown);
        txtOutOFDown = findViewById(R.id.txtOutOFDown);
        txtCountCircleDown = findViewById(R.id.txtCountCircleDown);
        imgLeftArrow = findViewById(R.id.imgLeftArrow);
        lytCount = findViewById(R.id.lytCount);

        webLoading = findViewById(R.id.webLoading);


        webTopNews = findViewById(R.id.webTopNews);

        CommonFunction.startWebView(RewardVideos.this, webTopNews, WEB_THE_DIRT_SHEET, webLoading);


        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        _userId = preferences.getString("User_Id", "");

        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        deviceId = md5(android_id).toUpperCase();


        i = Integer.parseInt(PreferenceHelper.getStringPreference(RewardVideos.this, COUNT_DOWN));
        txtOutOFDown.setText(PreferenceHelper.getStringPreference(RewardVideos.this, COUNT_DOWN) + "/" + 5);


        if (PreferenceHelper.getStringPreference(RewardVideos.this, COUNT_DOWN).equals("5")) {
            txtOutOFDown.setText("Done");
            txtCountDown.setText("Today task has been completed !");
            txtCountDown.setTextColor(getResources().getColor(R.color.material_green));

        } else {

            txtOutOFDown.setText(PreferenceHelper.getStringPreference(RewardVideos.this, COUNT_DOWN) + "/" + 5);
        }


        if (!PreferenceHelper.isGetFlag(RewardVideos.this, REWARDSESSION)) {
            CommonFunction.reWardDialogPop(RewardVideos.this, false, PreferenceHelper.getStringPreference(RewardVideos.this, COUNT_DOWN) + "/5", "Today task is incomplete !");

        } else {

            CommonFunction.reWardDialogPop(RewardVideos.this, false, "Done", "Today task has been completed !");
            imgRightArrow.setVisibility(View.INVISIBLE);
            imgLeftArrow.setVisibility(View.VISIBLE);
        }

    }

}

