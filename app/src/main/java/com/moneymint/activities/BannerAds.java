package com.moneymint.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.moneymint.ApiHandler.VolleySingleton;
import com.moneymint.CommonFunction;
import com.moneymint.LocalSaved.PreferenceHelper;
import com.moneymint.R;
import com.moneymint.configs.Configs;
import com.moneymint.utils.InternetConnectivity.BaseActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.moneymint.CommonFunction.getCurrentDate;
import static com.moneymint.configs.Configs.KEY_CLIENT_ID;
import static com.moneymint.configs.Configs.clientId;
import static com.moneymint.utils.Constant.ANDROID_ID;
import static com.moneymint.utils.Constant.BANNER_COUNT_DOWN;
import static com.moneymint.utils.Constant.BANNER_REWARDSESSION;
import static com.moneymint.utils.Constant.BANNER_SESSION_DATE;
import static com.moneymint.utils.Constant.TODAY_ATTEMPT_BANNER;
import static com.moneymint.utils.Constant.TODAY_TASK_STATUS_BANNER;
import static com.moneymint.utils.Constant.USER_CAST;
import static com.moneymint.utils.Constant.WEB_THE_DIRT_SHEET;

public class BannerAds extends BaseActivity {
    private Toolbar toolbar;
    private ImageView imgRightArrow,img_timer,imgLeftArrow;
    private CountDownTimer countDownTimer;
    private TextView txtCountDown,txtOutOFDown,txtCountCircleDown;
    private RelativeLayout lytCount;
    private WebView webTopNews;
    public static final String KEY_USER_ID = "customer_id";
    int i=0;
    int numberOfAttempt=1;

    String _userId;
    boolean isVideoCompleted=true;
    boolean isCounterFinished=true;
    boolean isReplay=true;
    private ProgressBar webLoading;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner_ads);

        toolbar= findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("Banner Ads");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        webLoading=findViewById(R.id.webLoading);

        final SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        _userId = preferences.getString("User_Id", "");
        imgRightArrow=findViewById(R.id.imgRightArrow);
        imgLeftArrow=findViewById(R.id.imgLeftArrow);
        img_timer=findViewById(R.id.img_timer);
        txtCountDown=findViewById(R.id.txtCountDown);
        txtOutOFDown=findViewById(R.id.txtOutOFDown);
        lytCount=findViewById(R.id.lytCount);
        txtCountDown=findViewById(R.id.txtCountDown);
        txtCountCircleDown=findViewById(R.id.txtCountCircleDown);

        numberOfAttempt=Integer.parseInt(PreferenceHelper.getStringPreference(BannerAds.this,TODAY_ATTEMPT_BANNER));

        i=Integer.parseInt(PreferenceHelper.getStringPreference(BannerAds.this,BANNER_COUNT_DOWN));

        if (PreferenceHelper.getStringPreference(BannerAds.this,TODAY_TASK_STATUS_BANNER).equalsIgnoreCase("false"))
        {

            if (numberOfAttempt<9)
            {
                if (PreferenceHelper.getStringPreference(BannerAds.this,BANNER_COUNT_DOWN).equals("5"))
                {
                    txtOutOFDown.setText("Done");
                    txtCountDown.setText("Today task has been completed !");
                    txtCountDown.setTextColor(getResources().getColor(R.color.material_green));

                }else {

                    txtOutOFDown.setText(PreferenceHelper.getStringPreference(BannerAds.this,BANNER_COUNT_DOWN)+"/"+5);

                }


                if (!PreferenceHelper.isGetFlag(BannerAds.this,BANNER_REWARDSESSION))
                {
                    CommonFunction.reWardDialogPop(BannerAds.this,true,PreferenceHelper.getStringPreference(BannerAds.this,BANNER_COUNT_DOWN)+"/5","Today task is incomplete !");

                }else {

                    CommonFunction.reWardDialogPop(BannerAds.this,true,"Done","Today task has been completed !");
                    imgRightArrow.setVisibility(View.INVISIBLE);
                    imgLeftArrow.setVisibility(View.VISIBLE);
                }

            }else{

                CommonFunction.reWardDialogPop(BannerAds.this,false,PreferenceHelper.getStringPreference(BannerAds.this,TODAY_ATTEMPT_BANNER),"Your all today attempts have finished!");

            }

        }else {

            CommonFunction.reWardDialogPop(BannerAds.this,false,"Done","Today task has been completed !");

        }



        imgRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (numberOfAttempt<9)
                {
                    numberOfAttempt++;
                    PreferenceHelper.setStringPreference(BannerAds.this,TODAY_ATTEMPT_BANNER,String.valueOf(numberOfAttempt));

                    if (i<5)
                    {
                        isReplay=false;


                    }else {

                        imgRightArrow.setVisibility(View.INVISIBLE);
                    }

                }else {

                    CommonFunction.reWardDialogPop(BannerAds.this,false,PreferenceHelper.getStringPreference(BannerAds.this,TODAY_ATTEMPT_BANNER),"Your all today attempts have finished!");
                }


            }
        });

        imgLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (numberOfAttempt<9)
                {
                    numberOfAttempt++;
                    PreferenceHelper.setStringPreference(BannerAds.this,TODAY_ATTEMPT_BANNER,String.valueOf(numberOfAttempt));
                    isReplay=true;

                }else {

                    CommonFunction.reWardDialogPop(BannerAds.this,false,PreferenceHelper.getStringPreference(BannerAds.this,TODAY_ATTEMPT_BANNER),"Your all today attempts have finished.\n Attempt Tomorrow.");

                }


            }
        });

        webTopNews=findViewById(R.id.webTopNews);

        CommonFunction.startWebView(BannerAds.this,webTopNews,WEB_THE_DIRT_SHEET,webLoading);

       // bannerAds(String.valueOf(5));

    }






    private void Counter()
    {
        // This function for Timer

        countDownTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                txtCountDown.setText("Task is on going: ");
                txtCountDown.setTextColor(getResources().getColor(R.color.yellow_500));
                lytCount.setVisibility(View.VISIBLE);
                txtCountCircleDown.setText(String.valueOf(millisUntilFinished / 1000));
                imgRightArrow.setVisibility(View.INVISIBLE);
                imgLeftArrow.setVisibility(View.VISIBLE);

                //   progressDialog.show();
            }

            public void onFinish() {

                isCounterFinished=true;

                if (isVideoCompleted && isCounterFinished) {
                    txtCountDown.setText("Play next banner ad or replay.");
                    txtCountDown.setVisibility(View.VISIBLE);
                    lytCount.setVisibility(View.GONE);
                    txtCountDown.setTextColor(getResources().getColor(R.color.green_500));

                    if (i<5)
                    {
                        imgRightArrow.setVisibility(View.VISIBLE);

                    }else {

                        imgRightArrow.setVisibility(View.INVISIBLE);
                    }

                    imgLeftArrow.setVisibility(View.VISIBLE);

                }else {
                    txtCountDown.setText("Ad banner is not completed. Please play again !");
                    txtCountDown.setVisibility(View.VISIBLE);
                    lytCount.setVisibility(View.GONE);
                    txtCountDown.setTextColor(getResources().getColor(R.color.colorAccent));
                    imgRightArrow.setVisibility(View.INVISIBLE);
                    imgLeftArrow.setVisibility(View.VISIBLE);
                }

                if (!isReplay)
                {

                    if (i<4)
                    {
                        i=i+1;
                        txtOutOFDown.setText(i+"/"+5);
                        PreferenceHelper.setStringPreference(BannerAds.this,BANNER_COUNT_DOWN,""+i);
                        PreferenceHelper.setStringPreference(BannerAds.this,BANNER_SESSION_DATE,getCurrentDate());

                    }else {
                        i=i+1;
                        txtOutOFDown.setText("Done");
                        txtCountDown.setText("Today task has been completed !");
                        imgRightArrow.setVisibility(View.INVISIBLE);

                        PreferenceHelper.isFlag(BannerAds.this,BANNER_REWARDSESSION,true);
                        PreferenceHelper.setStringPreference(BannerAds.this,BANNER_COUNT_DOWN,""+i);
                        PreferenceHelper.setStringPreference(BannerAds.this,BANNER_SESSION_DATE,getCurrentDate());

                        CommonFunction.reWardDialogPop(BannerAds.this,true,PreferenceHelper.getStringPreference(BannerAds.this,BANNER_COUNT_DOWN),"Your task has been completed !");
                        bannerAds(String.valueOf(5));

                    }
                }


            }
        }.start();
    }


    //////////////////reWardAPI /////////////////////////////////////////////
    private void bannerAds(final String userCast)
    {
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        final String m_androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e("TAG",m_androidId);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_ADDBANNER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {
                    progressDialog.dismiss();
                    Log.e("forgetResponse0",response);
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("error").equalsIgnoreCase("false"))
                    {
                        Toast.makeText(BannerAds.this, "Moon has Uploaded", Toast.LENGTH_SHORT).show();
                    }else
                    {
                        Toast.makeText(BannerAds.this, "Moon has not Uploaded", Toast.LENGTH_SHORT).show();

                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(BannerAds.this,"Some error occurred",Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_USER_ID, _userId);
                params.put(KEY_CLIENT_ID, clientId(BannerAds.this));
                params.put(USER_CAST, userCast);
                params.put(ANDROID_ID,m_androidId);

                return params;
            }

        };

        VolleySingleton.getInstance(BannerAds.this).addToRequestQueue(stringRequest);

    }



}
