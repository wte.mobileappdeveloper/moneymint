package com.moneymint.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneymint.R;
import com.moneymint.configs.Configs;
import com.moneymint.models.ReferReportModel;
import com.moneymint.models.SelfRecodModel;
import com.moneymint.recyclerAdapter.ReferRecordAdapter;
import com.moneymint.recyclerAdapter.SelfRecordAdapter;
import com.moneymint.utils.InternetConnectivity.BaseActivity;
import com.moneymint.viewPager.ApprovedTabAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.moneymint.activities.Home.KEY_USERID;
import static com.moneymint.configs.Configs.KEY_CLIENT_ID;
import static com.moneymint.fragments.ReferFragment.refer_nodata;
import static com.moneymint.fragments.ReferFragment.refer_recyclerview;
import static com.moneymint.fragments.SelfHistory.self_nodata;
import static com.moneymint.fragments.SelfHistory.self_recyclerview;

public class MonthlyTaskWiseHistory extends BaseActivity implements TabLayout.OnTabSelectedListener {
    TabLayout tabLayout;
    ViewPager viewPager;
    TextView balance;
    private Toolbar toolbar;
    private ProgressDialog progressDialog;
    FloatingActionButton fab;
    private int cYear, cMonth, cDay;
    String strMonth;
//    public String current_month;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval_payment_history);

        // toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        fab=findViewById(R.id.fab);

        toolbar.setTitle("Monthly History of " +getIntent().getStringExtra("month"));

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        balance=findViewById(R.id.earnedPts);
        progressDialog=new ProgressDialog(MonthlyTaskWiseHistory.this);

        // Page adapter

        tabLayout.addTab(tabLayout.newTab().setText("Self Income"));
        tabLayout.addTab(tabLayout.newTab().setText("Referral Income"));
        tabLayout.addTab(tabLayout.newTab().setText("Moon Income"));


        ApprovedTabAdapter adapter = new ApprovedTabAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setAdapter(adapter);
        tabLayout.setOnTabSelectedListener(this);

        if (getIntent().getStringExtra("status").equalsIgnoreCase("P")){
            fab.hide();
        }else {
            fab.show();
        }
        parsemonthlyHistory(getIntent().getStringExtra("status"),month(getIntent().getStringExtra("month")));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
                try {
                    Date date = sdf.parse(new SimpleDateFormat("MMM-yyyy").format(new Date()));

                    c.setTime(date);
                } catch (Exception e) {

                }
                cYear = c.get(Calendar.YEAR);
                cMonth = c.get(Calendar.MONTH);
                cDay = c.get(Calendar.DAY_OF_MONTH);
                strMonth=c.getDisplayName(Calendar.MONTH,Calendar.LONG, Locale.getDefault());
                DatePickerDialog datePickerDialog = new DatePickerDialog(MonthlyTaskWiseHistory.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                try {
                                    SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy",Locale.getDefault());
                                    Date date = sdf.parse((1+monthOfYear)  + "-" + year);
                                    String currentMonth=new SimpleDateFormat("MMM-yyyy",Locale.getDefault()).format(date);
                                    toolbar.setTitle("Monthly History of " +currentMonth);
                                    parsemonthlyHistory("R",month(currentMonth));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    Log.e("date error",e.toString());
                                }



                            }


                        }, cYear, cMonth, cDay);






                datePickerDialog.show();
            }
        });
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {



    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void parsemonthlyHistory(final String status, final String month){

        final ProgressDialog pd=new ProgressDialog(MonthlyTaskWiseHistory.this);
        pd.setMessage("loading...");
        pd.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_monthwiseReport, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("AprvlPmnt mnthwse hstry",response);
                try {

                    pd.dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("error").equals("false")) {


                        ArrayList<ReferReportModel> list = new ArrayList<>();
                        ArrayList<SelfRecodModel> list2 = new ArrayList<>();
                        list.clear();
                        list2.clear();



                        if (jsonObject.optJSONArray("datareferal")!=null){
                            JSONArray jsonArray=jsonObject.getJSONArray("datareferal");
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                ReferReportModel referReportModel=new ReferReportModel();

                                referReportModel.setMeberPoints(jsonObject1.getString("referalincome"));
                                referReportModel.setTaskName(jsonObject1.getString("camping_name"));
                                referReportModel.setMemberName(jsonObject1.getString("customer_name"));
                                referReportModel.setEntryDate(jsonObject1.getString("entrydate"));

                                list.add(referReportModel);
                            }

                        }


                        if (jsonObject.optJSONArray("dataself")!=null) {
                            JSONArray jsonArray2 = jsonObject.getJSONArray("dataself");

                            for (int i = 0; i < jsonArray2.length(); i++) {
                                JSONObject jsonObject1 = jsonArray2.getJSONObject(i);
                                SelfRecodModel selfRecodModel=new SelfRecodModel();

                                selfRecodModel.setSelfTasks(jsonObject1.getString("camping_name"));
                                selfRecodModel.setApprovDate(jsonObject1.getString("entrydate"));
                                selfRecodModel.setApprovPoints(jsonObject1.getString("selfincome"));

                                list2.add(selfRecodModel);

                            }
                        }



                        SelfRecordAdapter selfRecordAdapter= new SelfRecordAdapter(MonthlyTaskWiseHistory.this, list2);

                        self_recyclerview.setAdapter(selfRecordAdapter);
                        self_recyclerview.setLayoutManager(new LinearLayoutManager(MonthlyTaskWiseHistory.this));
                        self_recyclerview.setHasFixedSize(true);
                        if (list2.size()==0){
                            Log.e("Monthlywise self size","0");
                            self_nodata.setVisibility(View.VISIBLE);
                        }else {
                            Log.e("Monthlywise self size","not 0");
                            self_nodata.setVisibility(View.GONE);
                        }


                        ReferRecordAdapter adapter= new ReferRecordAdapter(MonthlyTaskWiseHistory.this, list);

                        refer_recyclerview.setAdapter(adapter);
                        refer_recyclerview.setLayoutManager(new LinearLayoutManager(MonthlyTaskWiseHistory.this));
                        refer_recyclerview.setHasFixedSize(true);
                        if (list.size()==0){
                            Log.e("Monthlywise refer size","0");
                            refer_nodata.setVisibility(View.VISIBLE);
                        }else {
                            Log.e("Monthlywise refer size","not 0");
                            refer_nodata.setVisibility(View.GONE);
                        }


                    }else {
                        refer_nodata.setVisibility(View.VISIBLE);
                        self_nodata.setVisibility(View.VISIBLE);

                    }

                } catch (Exception e) {
                    Log.e("errormonthwise",e.toString());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                pd.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                String userId=getSharedPreferences("login", Context.MODE_PRIVATE).getString("User_Id", "");
                params.put(KEY_CLIENT_ID,Configs.clientId(MonthlyTaskWiseHistory.this));
                params.put(KEY_USERID, userId);
                params.put("status", status);
                params.put("month_name", month);
                Log.e("Approvehist params",params.toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


    private String month(String month){


        String mnth=month.substring(0,3);

        String year=month.substring(4);
        String wordMonth=null;

        if (mnth.equals("Jan")){
            wordMonth="01";

        }else if (mnth.equals("Feb")){
            wordMonth="02";

        }else if (mnth.equals("Mar")){
            wordMonth="03";

        }else if (mnth.equals("Apr")){
            wordMonth="04";

        }else if (mnth.equals("May")){
            wordMonth="05";

        }else if (mnth.equals("Jun")){
            wordMonth="06";

        }else if (mnth.equals("Jul")){
            wordMonth="07";

        }else if (mnth.equals("Aug")){
            wordMonth="08";

        }else if (mnth.equals("Sep")){
            wordMonth="09";

        }else if (mnth.equals("Oct")){
            wordMonth="10";

        }else if (mnth.equals("Nov")){
            wordMonth="11";

        }else if (mnth.equals("Dec")){
            wordMonth="12";

        }





        return wordMonth+"-"+year;
    }





}
