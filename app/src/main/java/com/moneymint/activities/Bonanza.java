package com.moneymint.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.moneymint.R;
import com.moneymint.recyclerAdapter.BonanzaAdapter;
import com.moneymint.utils.InternetConnectivity.BaseActivity;

public class Bonanza extends BaseActivity {
    RecyclerView bonanzarecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bonanza);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toggle_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Bonanza");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        bonanzarecycler=findViewById(R.id.bonanzaRecycler);

        bonanza();
    }

    private void bonanza(){
        BonanzaAdapter bonanzaAdapter=new BonanzaAdapter(this);
        bonanzarecycler.setAdapter(bonanzaAdapter);
        bonanzarecycler.setLayoutManager(new LinearLayoutManager(this));
        bonanzarecycler.setHasFixedSize(true);
    }
}
