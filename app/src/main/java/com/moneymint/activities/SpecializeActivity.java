package com.moneymint.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.moneymint.ApiHandler.VolleySingleton;
import com.moneymint.CommonFunction;
import com.moneymint.LocalSaved.PreferenceHelper;
import com.moneymint.R;
import com.moneymint.configs.Configs;
import com.moneymint.utils.InternetConnectivity.BaseActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.moneymint.CommonFunction.getCurrentDate;
import static com.moneymint.configs.Configs.KEY_CLIENT_ID;
import static com.moneymint.configs.Configs.clientId;
import static com.moneymint.utils.Constant.ANDROID_ID;
import static com.moneymint.utils.Constant.SPECL_COUNT_DOWN;
import static com.moneymint.utils.Constant.SPEC_REWARDSESSION;
import static com.moneymint.utils.Constant.SPEC_SESSION_DATE;
import static com.moneymint.utils.Constant.TODAY_ATTEMPT_SPEC;
import static com.moneymint.utils.Constant.TODAY_TASK_STATUS_SPEC;
import static com.moneymint.utils.Constant.USER_CAST;
import static com.moneymint.utils.Constant.WEB_THE_DIRT_SHEET;

public class SpecializeActivity extends BaseActivity {

    private Toolbar toolbar;

    public static final String KEY_USER_ID = "customer_id";
    private ImageView imgRightArrow,img_timer,imgLeftArrow;
    private CountDownTimer countDownTimer;
    private TextView txtCountDown,txtOutOFDown,txtCountCircleDown;
    int i=0;
    private RelativeLayout lytCount;
    private WebView mWebview ;
    String _userId;
    boolean isVideoCompleted=true;
    boolean isCounterFinished=true;
    boolean isReplay=true;
    private ProgressDialog progressDialog;
    private ProgressBar webLoading;
    int numberOfAttempt=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specialize);

        toolbar= findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("Interstitial");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        webLoading=findViewById(R.id.webLoading);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        // this is for admob



        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        _userId = preferences.getString("User_Id", "");

        mWebview  = findViewById(R.id.webTopNews);

        CommonFunction.startWebView(SpecializeActivity.this,mWebview,WEB_THE_DIRT_SHEET,webLoading);


        imgRightArrow=findViewById(R.id.imgRightArrow);
        img_timer=findViewById(R.id.img_timer);
        imgLeftArrow=findViewById(R.id.imgLeftArrow);
        txtCountDown=findViewById(R.id.txtCountDown);
        txtOutOFDown=findViewById(R.id.txtOutOFDown);
        txtCountCircleDown=findViewById(R.id.txtCountCircleDown);
        lytCount=findViewById(R.id.lytCount);

        numberOfAttempt=Integer.parseInt(PreferenceHelper.getStringPreference(SpecializeActivity.this,TODAY_ATTEMPT_SPEC));
        i=Integer.parseInt(PreferenceHelper.getStringPreference(SpecializeActivity.this,SPECL_COUNT_DOWN));

        if (PreferenceHelper.getStringPreference(SpecializeActivity.this,TODAY_TASK_STATUS_SPEC).equalsIgnoreCase("false"))
        {
            if (numberOfAttempt<9)
            {
                if (PreferenceHelper.getStringPreference(SpecializeActivity.this,SPECL_COUNT_DOWN).equals("5"))
                {
                    txtOutOFDown.setText("Done");
                    txtCountDown.setText("Today task has been completed !");
                    txtCountDown.setTextColor(getResources().getColor(R.color.material_green));

                }else {

                    txtOutOFDown.setText(PreferenceHelper.getStringPreference(SpecializeActivity.this,SPECL_COUNT_DOWN)+"/"+5);

                }

                if (!PreferenceHelper.isGetFlag(SpecializeActivity.this,SPEC_REWARDSESSION))
                {
                    CommonFunction.reWardDialogPop(SpecializeActivity.this,true,PreferenceHelper.getStringPreference(SpecializeActivity.this,SPECL_COUNT_DOWN)+"/5","Today task is incomplete !");

                }else {

                    CommonFunction.reWardDialogPop(SpecializeActivity.this,true,"Done","Today task has been completed !");

                    imgRightArrow.setVisibility(View.INVISIBLE);
                    imgLeftArrow.setVisibility(View.VISIBLE);
                }

            }else {

                CommonFunction.reWardDialogPop(SpecializeActivity.this,false,PreferenceHelper.getStringPreference(SpecializeActivity.this,TODAY_ATTEMPT_SPEC),"Your all today attempts have finished.\n Attempt Tomorrow.");

            }
        }else {

            CommonFunction.reWardDialogPop(SpecializeActivity.this,false,"Done","Today task has been completed !");

        }





    }

}
