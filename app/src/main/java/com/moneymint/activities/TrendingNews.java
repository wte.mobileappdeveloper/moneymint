package com.moneymint.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.moneymint.ApiHandler.VolleySingleton;
import com.moneymint.BuildConfig;
import com.moneymint.CommonFunction;
import com.moneymint.LocalSaved.PreferenceHelper;
import com.moneymint.R;
import com.moneymint.configs.Configs;
import com.moneymint.utils.InternetConnectivity.BaseActivity;
import com.startapp.android.publish.adsCommon.StartAppAd;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.moneymint.CommonFunction.getCurrentDate;
import static com.moneymint.configs.Configs.KEY_CLIENT_ID;
import static com.moneymint.configs.Configs.clientId;
import static com.moneymint.utils.Constant.ANDROID_ID;
import static com.moneymint.utils.Constant.KEY_COUNT;
import static com.moneymint.utils.Constant.KEY_COUNT_TRENDING;
import static com.moneymint.utils.Constant.KEY_DATE;
import static com.moneymint.utils.Constant.KEY_DATE_TRENDING;
import static com.moneymint.utils.Constant.TOP_TRENDING_COUNT_DOWN;
import static com.moneymint.utils.Constant.TOP_TRENDING_SESSION_DATE;
import static com.moneymint.utils.Constant.USER_CAST;
import static com.moneymint.utils.Constant.WEB_THE_DIRT_SHEET;

public class TrendingNews extends AppCompatActivity {
    private Toolbar toolbar;
    private WebView webTopNews;
    FloatingActionButton floatNextNews;
    private CountDownTimer countDownTimer;
    boolean isBack=false;
    int count=1 ;
    public static final String KEY_USER_ID = "customer_id";
    String date,_userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trending_news);
        toolbar= findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("Fdelight.com");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        ////get value from shareprefrence....
        count=PreferenceHelper.getIntPreference(TrendingNews.this,KEY_COUNT_TRENDING);
        Log.e("getres", String.valueOf(count));
        date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        Log.e("current",date);

        date=PreferenceHelper.getStringPreference(TrendingNews.this,KEY_DATE_TRENDING);
        final SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        _userId = preferences.getString("User_Id", "");

        floatNextNews=findViewById(R.id.floatNextNews);
        webTopNews=findViewById(R.id.webTopNews);
        webTopNews.loadUrl(getIntent().getExtras().getString("link"));
        WebSettings webSettings = webTopNews.getSettings();
        webSettings.setJavaScriptEnabled(true);


        floatNextNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Money Mint");
                    String shareMessage= getIntent().getExtras().getString("link")+"\n\n"+"Intall App";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch(Exception e) {
                    //e.toString();
                }

            }
        });


        Counter();
    }
    private void Counter()
    {
        countDownTimer = new CountDownTimer(40000, 1000) {

            public void onTick(long millisUntilFinished) {

                //   progressDialog.show();
            }

            public void onFinish() {
                count++;
                Log.e("res", String.valueOf(count));
                PreferenceHelper.setIntPreference(TrendingNews.this,KEY_COUNT_TRENDING,count);
                PreferenceHelper.setStringPreference(TrendingNews.this,KEY_DATE_TRENDING,date);

                if (count==5){
                    Reward(String.valueOf(5));
                }

                isBack=true;
//                if (mInterstitialAd.isLoaded()) {
//                    mInterstitialAd.show();
//                }

            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        if (isBack)
        {
            super.onBackPressed();

        }
    }
    //// after 5 visit give rewas...
    public void  Reward(  final String userCast){
        final String m_androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, Configs.url_SPECIAL_VIDEO, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("res",response);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.optString("error").equalsIgnoreCase("false")){
                        Toast.makeText(TrendingNews.this, "points are approved", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(TrendingNews.this, "points are not approved", Toast.LENGTH_SHORT).show();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(TrendingNews.this,"Some error occurred",Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> params=new HashMap<>();
                params.put(KEY_USER_ID,_userId);
                params.put(KEY_CLIENT_ID,clientId(TrendingNews.this));
                params.put(USER_CAST,userCast);
                params.put(ANDROID_ID,m_androidId);
                return params;
            }
        };
        VolleySingleton.getInstance(TrendingNews.this).addToRequestQueue(stringRequest);
    }

}
