package com.moneymint.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.moneymint.R;
import com.moneymint.utils.InternetConnectivity.BaseActivity;


public class QuizResult extends BaseActivity {
    TextView correcthead,correct,incorrect,missed,accuracy;
    int correct_s,missed_s,incorrect_s;
    float accuracy_s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_result);

        correcthead=findViewById(R.id.scoreheadimg);
        correct=findViewById(R.id.correctScore);
        incorrect=findViewById(R.id.incorrectScore);
        missed=findViewById(R.id.missedscore);
        accuracy=findViewById(R.id.acurracy);

        correct_s=getIntent().getIntExtra("correct",0);
        missed_s=getIntent().getIntExtra("missed",0);
        incorrect_s=15-correct_s-missed_s;
        accuracy_s=correct_s/0.15f;

        ////////////////toolbaar////////////////////

        Toolbar toolbar= (Toolbar) findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("Result");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(QuizResult.this,Home.class));
            }
        });

        TextView toolUname=findViewById(R.id.toolbar_uName);
        SharedPreferences sharedpreferences = getSharedPreferences("user_name", Context.MODE_PRIVATE);
        toolUname.setText(sharedpreferences.getString("uName",""));


        setData();
    }



    public void setData(){
        correcthead.setText("Your score is: "+correct_s+" points");
        correct.setText(correct_s+"\nCorrect");
        incorrect.setText(incorrect_s+"\nIncorrect");
        missed.setText(missed_s+"\nMissed");
        accuracy.setText(Math.round(accuracy_s)+"%\nAccuracy");
    }
}
