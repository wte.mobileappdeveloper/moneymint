package com.moneymint.activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.moneymint.R;
import com.moneymint.models.ViralNewsModel;
import com.moneymint.utils.InternetConnectivity.BaseActivity;
import com.moneymint.viewPager.News_ViewPager;

import java.util.ArrayList;


public class ViralNews extends BaseActivity {
    ViewPager viewPager;
    ImageView settings;
    public static ImageView rightarrow,leftarrow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viral_news);



        ////////////////toolbaar////////////////////

        Toolbar toolbar= (Toolbar) findViewById(R.id.toggle_toolbar);
        toolbar.setTitle("Viral News");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        rightarrow=findViewById(R.id.right_arrow);
        leftarrow=findViewById(R.id.left_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        settings=findViewById(R.id.setting_news);

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ViralNews.this,SettingNewsActivity.class));
            }
        });

        ArrayList<ViralNewsModel> virallist;
        virallist= (ArrayList<ViralNewsModel>) getIntent().getSerializableExtra("virallist");


        viewPager=findViewById(R.id.news_pager);
        viewPager.setAdapter(new News_ViewPager(ViralNews.this,virallist));
        viewPager.setCurrentItem(getIntent().getExtras().getInt("position"));

        changeNewsForward();


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position==0){
                    leftarrow.setVisibility(View.GONE);
                }
                else if (position==9){
                    rightarrow.setVisibility(View.GONE);
                }
                else {
                    leftarrow.setVisibility(View.VISIBLE);
                    rightarrow.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void changeNewsForward(){

        if (viewPager.getCurrentItem()==0){
            leftarrow.setVisibility(View.GONE);
        }
        else if (viewPager.getCurrentItem()==9){
            rightarrow.setVisibility(View.GONE);
        }

        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentPos=viewPager.getCurrentItem();
                if (currentPos>0){
                    viewPager.setCurrentItem(currentPos-1);
                    rightarrow.setVisibility(View.VISIBLE);
                    if (currentPos==1){
                        leftarrow.setVisibility(View.GONE);
                    }
                }
            }
        });
        rightarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentPos=viewPager.getCurrentItem();
                if (currentPos<9){
                    viewPager.setCurrentItem(currentPos+1);
                    leftarrow.setVisibility(View.VISIBLE);
                    if (currentPos==8){
                        rightarrow.setVisibility(View.GONE);
                    }
                }
            }
        });



    }



}
