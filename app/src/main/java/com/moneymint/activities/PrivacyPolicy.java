package com.moneymint.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.moneymint.R;

public class PrivacyPolicy extends AppCompatActivity {


    WebView webViews;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        toolbar = (Toolbar) findViewById(R.id.toggle_toolbar);
        toolbar.setTitle("Privacy Policy");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        webViews=findViewById(R.id.webViews);
        webViews.loadData(data(),"text/html", "utf-8");
    }


    String data(){
        return  "<!DOCTYPE html>\n" +
                "<html lang=\"en-US\">\n" +
                "<head>\n" +
                "<meta charset=\"utf-8\" />\n" +
                "<meta name=viewport content=\"width=device-width, initial-scale=1.0\" />\n" +
                "<!-- feeds & pingback -->\n" +
                "<link rel=\"profile\" href=\"http://gmpg.org/xfn/11\" />\n" +
                "<title>PRIVACY POLICY AND DATA POLICY FOR MONEYMINT APPLICATION &#8211; MONEYMINT</title>\n" +
                "<meta name='robots' content='noindex,follow' />\n" +
                "<link rel='dns-prefetch' href='//fonts.googleapis.com' />\n" +
                "<link rel='dns-prefetch' href='//s.w.org' />\n" +
                "<link href='https://fonts.gstatic.com' crossorigin rel='preconnect' />\n" +
                "<style type=\"text/css\">\n" +
                "img.wp-smiley,\n" +
                "img.emoji {\n" +
                "\tdisplay: inline !important;\n" +
                "\tborder: none !important;\n" +
                "\tbox-shadow: none !important;\n" +
                "\theight: 1em !important;\n" +
                "\twidth: 1em !important;\n" +
                "\tmargin: 0 .07em !important;\n" +
                "\tvertical-align: -0.1em !important;\n" +
                "\tbackground: none !important;\n" +
                "\tpadding: 0 !important;\n" +
                "}\n" +
                "</style>\n" +
                "\n" +
                "<link rel='stylesheet' id='default_font-css'  href='//fonts.googleapis.com/css?family=Roboto%3A400%2C700%2C400i%2C700i&#038;subset=latin%2Ccyrillic&#038;ver=4.9.10' type='text/css' media='all' />\n" +
                "<style type=\"text/css\"> #main_header,.is-sticky .logo_section_wrap{background-color:#ffffff !important}.header-top{border:none;} .widget .title:after{border-bottom:2px solid #004b5e;}.rehub-main-color-border,nav.top_menu > ul > li.vertical-menu.border-main-color .sub-menu,.rh-main-bg-hover:hover,.wp-block-quote,ul.def_btn_link_tabs li.active a,.wp-block-pullquote{border-color:#004b5e;}.wpsm_promobox.rehub_promobox{border-left-color:#004b5e!important;}.top_rating_block .top_rating_item .rating_col a.read_full,.color_link{color:#004b5e !important;}.search-header-contents{border-top-color:#004b5e;}.wpb_content_element.wpsm-tabs.n_b_tab .wpb_tour_tabs_wrapper .wpb_tabs_nav .ui-state-active a{border-bottom:3px solid #004b5e !important}.featured_slider:hover .score,.top_chart_controls .controls:hover,article.post .wpsm_toplist_heading:before{border-color:#004b5e;}.btn_more:hover,.small_post .overlay .btn_more:hover,.tw-pagination .current{border:1px solid #004b5e;color:#fff}.wpsm-tabs ul.ui-tabs-nav .ui-state-active a,.rehub_woo_review .rehub_woo_tabs_menu li.current{border-top:3px solid #004b5e;}.wps_promobox{border-left:3px solid #004b5e;}.gallery-pics .gp-overlay{box-shadow:0 0 0 4px #004b5e inset;}.post .rehub_woo_tabs_menu li.current,.woocommerce div.product .woocommerce-tabs ul.tabs li.active{border-top:2px solid #004b5e;}.rething_item a.cat{border-bottom-color:#004b5e}nav.top_menu ul li ul{border-bottom:2px solid #004b5e;}.widget.deal_daywoo{border:3px solid #004b5e;padding:20px;background:#fff;}.deal_daywoo .wpsm-bar-bar{background-color:#004b5e !important} #buddypress div.item-list-tabs ul li.selected a span,#buddypress div.item-list-tabs ul li.current a span,#buddypress div.item-list-tabs ul li a span,.user-profile-div .user-menu-tab > li.active > a,.user-profile-div .user-menu-tab > li.active > a:focus,.user-profile-div .user-menu-tab > li.active > a:hover,.slide .news_cat a,.news_in_thumb:hover .news_cat a,.news_out_thumb:hover .news_cat a,.col-feat-grid:hover .news_cat a,.alphabet-filter .return_to_letters span,.carousel-style-deal .re_carousel .controls,.re_carousel .controls:hover,.openedprevnext .postNavigation a,.postNavigation a:hover,.top_chart_pagination a.selected,.flex-control-paging li a.flex-active,.flex-control-paging li a:hover,.widget_edd_cart_widget .edd-cart-number-of-items .edd-cart-quantity,.btn_more:hover,.news_out_tabs > ul > li:hover,.news_out_tabs > ul > li.current,.featured_slider:hover .score,#bbp_user_edit_submit,.bbp-topic-pagination a,.bbp-topic-pagination a,.widget.tabs > ul > li:hover,.custom-checkbox label.checked:after,.slider_post .caption,ul.postpagination li.active a,ul.postpagination li:hover a,ul.postpagination li a:focus,.top_theme h5 strong,.re_carousel .text:after,.widget.tabs .current,#topcontrol:hover,.main_slider .flex-overlay:hover a.read-more,.rehub_chimp #mc_embed_signup input#mc-embedded-subscribe,#rank_1.top_rating_item .rank_count,#toplistmenu > ul li:before,.rehub_chimp:before,.wpsm-members > strong:first-child,.r_catbox_btn,.wpcf7 .wpcf7-submit,.comm_meta_wrap .rh_user_s2_label,.wpsm_pretty_hover li:hover,.wpsm_pretty_hover li.current,.rehub-main-color-bg,.togglegreedybtn:after,.rh-bg-hover-color:hover .news_cat a,.rh-main-bg-hover:hover,.rh_wrapper_video_playlist .rh_video_currently_playing,.rh_wrapper_video_playlist .rh_video_currently_playing.rh_click_video:hover,.rtmedia-list-item .rtmedia-album-media-count,.tw-pagination .current,.dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li.active,.dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li:hover,.dokan-dashboard .dokan-dash-sidebar ul.dokan-dashboard-menu li.dokan-common-links a:hover,#ywqa-submit-question,.woocommerce .widget_price_filter .ui-slider .ui-slider-range,.rh-hov-bor-line > a:after,nav.top_menu > ul:not(.off-canvas) > li > a:after{background:#004b5e;}@media (max-width:767px){.postNavigation a{background:#004b5e;}}.rh-main-bg-hover:hover{color:#fff !important} a,.carousel-style-deal .deal-item .priced_block .price_count ins,nav.top_menu ul li.menu-item-has-children ul li.menu-item-has-children > a:before,.top_chart_controls .controls:hover,.flexslider .fa-pulse,.footer-bottom .widget .f_menu li a:hover,.comment_form h3 a,.bbp-body li.bbp-forum-info > a:hover,.bbp-body li.bbp-topic-title > a:hover,#subscription-toggle a:before,#favorite-toggle a:before,.aff_offer_links .aff_name a,.rh-deal-price,.commentlist .comment-content small a,.related_articles .title_cat_related a,article em.emph,.campare_table table.one td strong.red,.sidebar .tabs-item .detail p a,.footer-bottom .widget .title span,footer p a,.welcome-frase strong,article.post .wpsm_toplist_heading:before,.post a.color_link,.categoriesbox:hover h3 a:after,.bbp-body li.bbp-forum-info > a,.bbp-body li.bbp-topic-title > a,.widget .title i,.woocommerce-MyAccount-navigation ul li.is-active a,.category-vendormenu li.current a,.deal_daywoo .title,.rehub-main-color,.wpsm_pretty_colored ul li.current a,.wpsm_pretty_colored ul li.current,.rh-heading-hover-color:hover h2 a,.rh-heading-hover-color:hover h3 a,.rh-heading-hover-color:hover h4 a,.rh-heading-hover-color:hover h5 a,.rh-heading-icon:before,.widget_layered_nav ul li.chosen a:before,.wp-block-quote.is-style-large p,ul.page-numbers li span.current,ul.page-numbers li a:hover,ul.page-numbers li.active a,.page-link > span:not(.page-link-title),blockquote:not(.wp-block-quote) p{color:#004b5e;} span.re_filtersort_btn:hover,span.active.re_filtersort_btn,.page-link > span:not(.page-link-title),.postimagetrend .title,.widget.widget_affegg_widget .title,.widget.top_offers .title,.widget.cegg_widget_products .title,header .header_first_style .search form.search-form [type=\"submit\"],header .header_eight_style .search form.search-form [type=\"submit\"],.more_post a,.more_post span,.filter_home_pick span.active,.filter_home_pick span:hover,.filter_product_pick span.active,.filter_product_pick span:hover,.rh_tab_links a.active,.rh_tab_links a:hover,.wcv-navigation ul.menu li.active,.wcv-navigation ul.menu li:hover a,form.search-form [type=\"submit\"],.rehub-sec-color-bg,input#ywqa-submit-question,input#ywqa-send-answer,.woocommerce button.button.alt{background:#e51d2a !important;color:#fff !important;outline:0}.widget.widget_affegg_widget .title:after,.widget.top_offers .title:after,.vc_tta-tabs.wpsm-tabs .vc_tta-tab.vc_active,.vc_tta-tabs.wpsm-tabs .vc_tta-panel.vc_active .vc_tta-panel-heading,.widget.cegg_widget_products .title:after{border-top-color:#e51d2a !important;}.page-link > span:not(.page-link-title){border:1px solid #e51d2a;}.page-link > span:not(.page-link-title),.header_first_style .search form.search-form [type=\"submit\"] i{color:#fff !important;}.rh_tab_links a.active,.rh_tab_links a:hover,.rehub-sec-color-border,nav.top_menu > ul > li.vertical-menu.border-sec-color > .sub-menu{border-color:#e51d2a}.rh_wrapper_video_playlist .rh_video_currently_playing,.rh_wrapper_video_playlist .rh_video_currently_playing.rh_click_video:hover{background-color:#e51d2a;box-shadow:1200px 0 0 #e51d2a inset;}.rehub-sec-color{color:#e51d2a} .price_count,.rehub_offer_coupon,#buddypress .dir-search input[type=text],.gmw-form-wrapper input[type=text],.gmw-form-wrapper select,.rh_post_layout_big_offer .priced_block .btn_offer_block,#buddypress a.button,.btn_more,#main_header .wpsm-button,#rh-header-cover-image .wpsm-button,#wcvendor_image_bg .wpsm-button,.rate-bar-bar,.rate-bar,.rehub-main-smooth,.re_filter_instore span.re_filtersort_btn:hover,.re_filter_instore span.active.re_filtersort_btn{border-radius:100px}.news .priced_block .price_count,.blog_string .priced_block .price_count,.main_slider .price_count{margin-right:5px}.right_aff .priced_block .btn_offer_block,.right_aff .priced_block .price_count{border-radius:0 !important}form.search-form.product-search-form input[type=\"text\"]{border-radius:4px 0 0 4px;}form.search-form [type=\"submit\"]{border-radius:0 4px 4px 0;}.rtl form.search-form.product-search-form input[type=\"text\"]{border-radius:0 4px 4px 0;}.rtl form.search-form [type=\"submit\"]{border-radius:4px 0 0 4px;}.woocommerce .products.grid_woo .product,.rh_offer_list .offer_thumb .deal_img_wrap,.rehub_chimp #mc_embed_signup input.email,#mc_embed_signup input#mc-embedded-subscribe,.grid_onsale,.def_btn,input[type=\"submit\"],input[type=\"button\"],input[type=\"reset\"],.wpsm-button,#buddypress div.item-list-tabs ul li a,#buddypress .standard-form input[type=text],#buddypress .standard-form textarea{border-radius:5px}.news-community,.review-top .overall-score,.rate_bar_wrap,.rh_offer_list,.woo-tax-logo,#buddypress form#whats-new-form,#buddypress div#invite-list,#buddypress #send-reply div.message-box,.rehub-sec-smooth,#wcfm-main-contentainer #wcfm-content,.wcfm_welcomebox_header{border-radius:8px}.review-top .overall-score span.overall-text{border-radius:0 0 8px 8px}.rh_offer_list .coupon_btn:before{right:-28px} .woocommerce .summary .masked_coupon,.woocommerce a.woo_loop_btn,.woocommerce input.button.alt,.woocommerce a.add_to_cart_button,.woocommerce-page a.add_to_cart_button,.woocommerce .single_add_to_cart_button,.woocommerce div.product form.cart .button,.woocommerce .checkout-button.button,.woofiltersbig .prdctfltr_buttons a.prdctfltr_woocommerce_filter_submit,.priced_block .btn_offer_block,.priced_block .button,.rh-deal-compact-btn,input.mdf_button,#buddypress input[type=\"submit\"],#buddypress input[type=\"button\"],#buddypress input[type=\"reset\"],#buddypress button.submit,.wpsm-button.rehub_main_btn,.wcv-grid a.button,input.gmw-submit,#ws-plugin--s2member-profile-submit,#rtmedia_create_new_album,input[type=\"submit\"].dokan-btn-theme,a.dokan-btn-theme,.dokan-btn-theme,#wcfm_membership_container a.wcfm_submit_button,.woocommerce button.button,.rehub-main-btn-bg{background:none #1e945f !important;color:#fff !important;border:none !important;text-decoration:none !important;outline:0;box-shadow:-1px 6px 19px rgba(30,148,95,0.25) !important;border-radius:100px !important;}.woocommerce a.woo_loop_btn:hover,.woocommerce input.button.alt:hover,.woocommerce a.add_to_cart_button:hover,.woocommerce-page a.add_to_cart_button:hover,.woocommerce a.single_add_to_cart_button:hover,.woocommerce-page a.single_add_to_cart_button:hover,.woocommerce div.product form.cart .button:hover,.woocommerce-page div.product form.cart .button:hover,.woocommerce .checkout-button.button:hover,.woofiltersbig .prdctfltr_buttons a.prdctfltr_woocommerce_filter_submit:hover,.priced_block .btn_offer_block:hover,.wpsm-button.rehub_main_btn:hover,#buddypress input[type=\"submit\"]:hover,#buddypress input[type=\"button\"]:hover,#buddypress input[type=\"reset\"]:hover,#buddypress button.submit:hover,.small_post .btn:hover,.ap-pro-form-field-wrapper input[type=\"submit\"]:hover,.wcv-grid a.button:hover,#ws-plugin--s2member-profile-submit:hover,input[type=\"submit\"].dokan-btn-theme:hover,a.dokan-btn-theme:hover,.dokan-btn-theme:hover,.rething_button .btn_more:hover,#wcfm_membership_container a.wcfm_submit_button:hover,.woocommerce button.button:hover,.rehub-main-btn-bg:hover{background:none #1e945f !important;color:#fff !important;box-shadow:-1px 6px 13px rgba(30,148,95,0.45) !important;border-color:transparent;}.woocommerce a.woo_loop_btn:active,.woocommerce .button.alt:active,.woocommerce a.add_to_cart_button:active,.woocommerce-page a.add_to_cart_button:active,.woocommerce a.single_add_to_cart_button:active,.woocommerce-page a.single_add_to_cart_button:active,.woocommerce div.product form.cart .button:active,.woocommerce-page div.product form.cart .button:active,.woocommerce .checkout-button.button:active,.woofiltersbig .prdctfltr_buttons a.prdctfltr_woocommerce_filter_submit:active,.wpsm-button.rehub_main_btn:active,#buddypress input[type=\"submit\"]:active,#buddypress input[type=\"button\"]:active,#buddypress input[type=\"reset\"]:active,#buddypress button.submit:active,.ap-pro-form-field-wrapper input[type=\"submit\"]:active,.wcv-grid a.button:active,#ws-plugin--s2member-profile-submit:active,input[type=\"submit\"].dokan-btn-theme:active,a.dokan-btn-theme:active,.dokan-btn-theme:active,.woocommerce button.button:active,.rehub-main-btn-bg:active{background:none #1e945f !important;box-shadow:0 1px 0 #999 !important;top:2px;color:#fff !important;}.rehub_btn_color{background-color:#1e945f;border:1px solid #1e945f;}.rething_button .btn_more{border:1px solid #1e945f;color:#1e945f;}.rething_button .priced_block.block_btnblock .price_count{color:#1e945f;font-weight:normal;}.widget_merchant_list .buttons_col{background-color:#1e945f !important;}@media (max-width:767px){#float-panel-woo-area{border-top:1px solid #1e945f}}.deal_daywoo .price{color:#1e945f}</style>\t<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>\n" +
                "\n" +
                "<!--[if lte IE 9]><link rel=\"stylesheet\" type=\"text/css\" href=\"http://www.MoneyMint.in/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css\" media=\"screen\"><![endif]-->\t\t<style type=\"text/css\" id=\"wp-custom-css\">\n" +
                "\n" +
                "@import url('https://fonts.googleapis.com/css?family=Raleway:300,400,700');\n" +
                "\n" +
                "body {\n" +
                "\tfont-family: 'Raleway', sans-serif;\n" +
                "}\n" +
                "\n" +
                "h1,h2,h3,h4,h5,h6,p,a {\n" +
                "\tfont-family: 'Raleway', sans-serif !important;\n" +
                "}\n" +
                "\n" +
                "header .logo img {\n" +
                "\theight: 85px;\n" +
                "  max-width: 118%;\n" +
                "}\n" +
                ".main-nav.dark_style {\n" +
                "\tbackground: #004b5e !important;\n" +
                "}\n" +
                ".vc_custom_1464714731453 {\n" +
                "\tbackground-color: rgba(0,0,0,0.1) !important;}\n" +
                "\n" +
                "#h3-custom,.tags {\n" +
                "\tfont-weight: 400;\n" +
                "}\n" +
                ".vc_custom_1553498076210,\n" +
                ".vc_custom_1553498110226 {\n" +
                "\t\tbox-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);\n" +
                "}\n" +
                "\n" +
                "nav.top_menu > ul > li > a {\n" +
                "    font: 400 16px/19px 'Raleway', sans-serif !important;\n" +
                "}\n" +
                "\n" +
                ".rehub_chimp_flat #mc_embed_signup input#mc-embedded-subscribe {\n" +
                "\tbackground: none #004b5e;\n" +
                "}\n" +
                "\n" +
                ".related_articles .related_title {\n" +
                "    font: 400 21px/21px 'Roboto', trebuchet ms;\n" +
                "    color: #ffffff;\n" +
                "    text-transform: uppercase;\n" +
                "    text-align: center;\n" +
                "    display: block; \n" +
                "    margin: 0 auto;\n" +
                "    background: #004b5e;\n" +
                "    padding: 5px 25px;\n" +
                "    margin-bottom: 20px;\n" +
                "\t  \n" +
                "}\n" +
                "\n" +
                ".related_articles {\n" +
                "\tbox-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);\n" +
                "}\n" +
                "\n" +
                "#comments .title_comments {\n" +
                "    font-size: 28px;\n" +
                "    font-weight: 400;\n" +
                "    text-align: center;\n" +
                "}\n" +
                "\n" +
                ".rehub-main-color-bg {\n" +
                "\tbackground: #dd3333;\n" +
                "}\n" +
                "\n" +
                ".product-categories li a{\n" +
                "\ttext-decoration: none !important;\n" +
                "}\n" +
                "\n" +
                "#mailpoet_form_1 .mailpoet_form {\n" +
                "\tdisplay: flex;\n" +
                "}\n" +
                "\n" +
                ".mailpoet_text {\n" +
                "\twidth: 400px !important;\n" +
                "  border: 1px solid #1d1b1b !important;\n" +
                "\n" +
                "\n" +
                "}\n" +
                ".mailpoet_text_label {\n" +
                "\tmargin-bottom: 5px;\n" +
                "}\n" +
                ".mailpoet_submit{\n" +
                "    height: 38px !important;\n" +
                "\twidth: 100px !important;\n" +
                "    background: #004b5e !important;\n" +
                "    color: #fff !important;\n" +
                "  \n" +
                "\tborder-radius: 0px !important;\n" +
                "\tmargin-top: 25px;\n" +
                "\n" +
                "}\n" +
                "</style>\n" +
                "\t\n" +
                "<body class=\"page-template-default page page-id-1193 woocommerce-no-js no_bg_wrap wpb-js-composer js-comp-ver-100 vc_responsive\">\n" +
                "\t\n" +
                "        <!-- CONTENT -->\n" +
                "<div class=\"rh-container\"> \n" +
                "    <div class=\"rh-content-wrap clearfix\">\n" +
                "        <!-- Main Side -->\n" +
                "        <div class=\"main-side page clearfix\" id=\"content\">\n" +
                "            <div class=\"title\"><h1>PRIVACY POLICY AND DATA POLICY FOR MoneyMint APPLICATION</h1></div>\n" +
                "            <article class=\"post\" id=\"page-1193\">       \n" +
                "                            <p>By accessing <strong>MoneyMint</strong> website or application services, you give us your consent to collect, store, and use the information you provide (including any changes whatsoever as provided by you) for any of the services that we provide. As we update and improve <strong>MoneyMint</strong> Services, this policy may change, so please refer to it periodically.</p>\n" +
                "<h4><strong>General Data Privacy Regulation (GDPR)</strong></h4>\n" +
                "<p>Please note that usage of this application is limited to users within India only. Only users with valid &amp; active Indian mobile number should download and install this app.<br />\n" +
                "As part of our efforts to provide you better access and control over the information which we collect and process about you, we require you to download and use the latest version of the App. If you continue to use an older version of the App, we would not be responsible or liable for collection, storage and processing of your information. You are required to update to the most current version of the App.</p>\n" +
                "<p><strong>Note:</strong> Our privacy policy may change at any time without prior notification. To make sure that you are aware of any changes, kindly review the policy periodically. This Privacy Policy shall apply uniformly to <strong>MoneyMint</strong> desktop website and <strong>MoneyMint</strong> mobile applications.</p>\n" +
                "<h3>Information we collect</h3>\n" +
                "<p><strong>A) Personal Information:</strong> To avail services on MoneyMint, you are required to provide following information for the registration/ login process.</p>\n" +
                "<p>(1) Mobile Number : We verify and validate the mobile number provided by you as all recharge based rewards are given based on this number. Other uses :</p>\n" +
                "<ul>\n" +
                "<li>Mobile number is also used in assisting you in case of any account related queries raised by you.</li>\n" +
                "<li>Mobile number is also used to communicate account related activities and custom offers occasionally.</li>\n" +
                "<li>We may send SMS with offers from our partners to you that are relevant to your profile. However we completely respect your privacy and we will not send you these without your consent.</li>\n" +
                "</ul>\n" +
                "<p>(2) Email address : Your email address is used to communicate service alerts (your activity on <strong>MoneyMint</strong> &amp; Service related communication).</p>\n" +
                "<p>Other uses : We send commercial or marketing messages which we deem to be relevant to you, with an option to unsubscribe in each email. We shall not send you any email communication if you choose to unsubscribe from these messages.</p>\n" +
                "<p>(3) Basic information such as your name, Date of birth, gender and state, city.</p>\n" +
                "<p>All required information is service dependent and the Company may use the above said user Information to maintain, protect, and improve MoneyMint Services and for developing new services.</p>\n" +
                "<p><strong>All the information provided by you is protected by MoneyMint and by no means will be leaked or shared with any third party.</strong></p>\n" +
                "<p>In case you choose to decline to submit your mobile number &amp; email address information on the app during registration, we will not be able to provide services on the app to you. We will make reasonable efforts to notify you of the same at the time of opening your account.</p>\n" +
                "<p>When you register with<strong> MoneyMint</strong> Services, we contact you from time to time about updating your personal information to provide you features that we believe may benefit / interest you. By agreeing to this privacy policy, you explicitly give us consent to use your information as described in this privacy policy. <strong>If you do not agree, please do not use or access MoneyMint services.</strong></p>\n" +
                "<p><strong>B) Sensitive Details</strong></p>\n" +
                "<p>After registration is complete, In order to process your request for a wire transfer, users can optionally choose to furnish their financial bank account information via the Profile section within <strong>MoneyMint</strong> app so that they can transfer their earned incentives into their bank accounts :</p>\n" +
                "<ul>\n" +
                "<li>Bank account number</li>\n" +
                "<li>Bank name</li>\n" +
                "<li>Branch Address</li>\n" +
                "<li>IFSC Code</li>\n" +
                "<li>PAN number</li>\n" +
                "</ul>\n" +
                "<p>Above information is completely voluntary and you can choose to not provide the same. The above Information as supplied by you enables you to transfer incentives into your bank accounts.</p>\n" +
                "<p><strong>Information we capture/ track from use of services or is obtained from third parties</strong></p>\n" +
                "<p>(This information is only captured from your device after you grant permission to MoneyMint app) We capture/ track information from your use of services. This information may be obtained from third parties in some cases. This information is used to provide best &amp; customized service to each of our users, improve overall service delivery, devise new services, identify and reduce fraud or Un- authorized use of our services and other purposes. All this information is captured after your explicit confirmation and permissions wherever required. Information tracked has been mentioned below to give details of the same.</p>\n" +
                "<p><strong>1. Device &amp; account information :</strong></p>\n" +
                "<p>We collect device-specific information (such as your hardware model, operating system version, unique device identifiers, and mobile network information including phone number and account information on phone).</p>\n" +
                "<p><strong>How we use this information:</strong></p>\n" +
                "<p>We primarily use this information to detect fraud &amp; unauthorized usage of <strong>MoneyMint</strong> services. Your device level information is also used in tracking your activity on <strong>MoneyMint</strong> for various actions &amp; tasks.</p>\n" +
                "<p><strong>2. Log information :</strong></p>\n" +
                "<p>When you use our services or view content provided by <strong>MoneyMint</strong>, we automatically collect and store certain information in server logs. This includes :</p>\n" +
                "<ul>\n" +
                "<li>Details of how you used our service.</li>\n" +
                "<li>Reading SMS Messages for the purpose of OTP verification &amp; to provide relevant offers based upon your interest.</li>\n" +
                "<li>Internet protocol address.</li>\n" +
                "<li>Device event information such as crashes, system activity, hardware settings, the date and time of your request and referral URL.</li>\n" +
                "<li>Cookies that may uniquely identify your browser or your MoneyMint Account.</li>\n" +
                "</ul>\n" +
                "<p><strong>How we use this information :</strong></p>\n" +
                "<p>We read SMS messages &amp; routing information only for mobile number verification purposes during registration/ signup on <strong>MoneyMint</strong>. We do not read messages sent by any service other than MoneyMint itself for registration purpose. And we may also read promotional or transactional SMS to provide you with relevant offers. We will never read peer to peer or personal. We use information collected from cookies and other technologies, like pixel tags etc. to improve your user experience and the overall quality of our services. We use multiple such 3rd party services like Google Analytics, Branch metrics etc. We may combine personal information from one service with information, including personal information, from other services to create a rich profile of you, to better your experience on MoneyMint.</p>\n" +
                "<p><strong>3. Location information :</strong></p>\n" +
                "<p>When you use <strong>MoneyMint</strong> services, we may collect and process information about your location. We use various technologies to determine location, including IP address, GPS, and other sensors that may, for example, provide <strong>MoneyMint</strong> with information on nearby devices, Wi-Fi access points and cell towers.</p>\n" +
                "<p><strong>How we use this information:</strong></p>\n" +
                "<p>We use location information to maintain &amp; improve services provided to you, to develop new services. These services include those provided to users within the app and via email communications that we shall send to you as mentioned above. We also use this information to offer you tailored &amp; customized offers within the app, website and emails.</p>\n" +
                "<p><strong>4. Unique application numbers :</strong></p>\n" +
                "<p>Certain services <strong>include</strong> a unique application number. This number and information about your installation (for example, the operating system type, application version number, etc) may be sent to <strong>MoneyMint</strong> when you install or uninstall that service.</p>\n" +
                "<p><strong>How we use this information:</strong></p>\n" +
                "<p>Operating system type &amp; version helps us in providing best experience of <strong>MoneyMint</strong> for your operating system along with customizing offers displayed to you. Information about Installing and uninstalling of other services helps us in keeping track of offers that are common between these and <strong>MoneyMint</strong> and thus giving you best experience and performance within the app.</p>\n" +
                "<p><strong>5. Local storage :</strong></p>\n" +
                "<p>We may collect and store information locally on your device using mechanisms such as application data, caches.</p>\n" +
                "<p><strong>How we use this information:</strong></p>\n" +
                "<p>This information is used to (a) maintain your login session on <strong>MoneyMint</strong> (b) this information may also be used from time-to-time to sync with <strong>MoneyMint</strong> servers and update the tasks and services that we provide to you within the app for your next usage.</p>\n" +
                "<p><strong>6. Cookies and similar technologies :</strong></p>\n" +
                "<p>We and our partners use various technologies to collect and store information when you visit an <strong>MoneyMint</strong> service, and this may include using cookies or similar technologies to identify your browser or device. We also use these technologies to collect and store information when you interact with services we offer to our partners, such as advertising services or MoneyMint features that may appear on other sites or various SDKs we use for product Analytics that help us analyze the traffic.</p>\n" +
                "<p><strong>How we use this information:</strong></p>\n" +
                "<p>We use information collected from cookies and other technologies like pixel tags, Post backs etc. to improve your user experience and the overall quality of our services. We use multiple tools &amp; 3rd party services like Google Analytics, Branch metrics etc. We may combine personal information from one service with information, including personal information, from other services to give you best experience within <strong>MoneyMint</strong>.</p>\n" +
                "<h3>INFORMATION WE SHARE :</h3>\n" +
                "<p>We do not share your personal information with companies, organizations and individuals outside of<strong> MoneyMint</strong> &amp; its partner companies unless one of the following circumstances applies:</p>\n" +
                "<p><strong>I) With your consent</strong><br />\n" +
                "We will share device level information with companies, organizations or individuals outside of <strong>MoneyMint</strong> when we have your consent to do so. When you agree to our Privacy Policy, you provide us consent to share your device level information (mainly used for analytics and marketing purpose). We do not share your personally identifiable information with any entity outside <strong>MoneyMint</strong>.</p>\n" +
                "<p><strong>Your information can be shared only under legal circumstances:</strong></p>\n" +
                "<p>We might share personal information with companies, organizations or individuals outside of <strong>MoneyMint</strong> &amp; its partner companies due to following legal reasons:</p>\n" +
                "<ul>\n" +
                "<li>Meet any applicable law, regulation, legal process or enforceable governmental request.</li>\n" +
                "<li>Enforce applicable Terms of Service, including investigation of potential violations.</li>\n" +
                "<li>Detect, prevent, or otherwise address fraud, security or technical issues.</li>\n" +
                "<li>Protect against harm to the rights, property or safety of MoneyMint &amp; its partner companies, our users or the public as required or permitted by law.</li>\n" +
                "</ul>\n" +
                "<p>We may share non-personally identifiable information with our partners like publishers, advertisers or connected sites. For example, we may share information publicly to show trends about the general use of our services.</p>\n" +
                "<p>Personally identifiable information may be transferred as an asset in connection with a merger or sale (including any transfers made as part of an insolvency or bankruptcy proceeding) involving all or part of our business or as part of a corporate reorganization, stock sale, or other change in control. Your visit and any dispute over privacy are subject to this policy and our terms and conditions, including limitations on damages.</p>\n" +
                "<p>We will only share your personal information with other companies or individuals outside of <strong>MoneyMint</strong> &amp; its partner companies in the following circumstances :</p>\n" +
                "<ul>\n" +
                "<li>As necessary to process your transactions and to maintain your account.</li>\n" +
                "<li>To complete your registration for a service provided by a third party</li>\n" +
                "</ul>\n" +
                "<p>The information that we collect, including information obtained from third parties, is shared with our affiliates, meaning other companies owned, controlled or are in partnership with MoneyMint. Our affiliates, which can be financial and non-financial entities, will use such information for their everyday business purposes.</p>\n" +
                "<p><strong>Links to third party sites / services</strong></p>\n" +
                "<p>MoneyMint Site/ App may include links to other websites or apps. Such sites are governed by their respective privacy policies, which are beyond our control. Once you click on those links, use of any information you provide is governed by the privacy policy of the operator of the site you are visiting.<br />\n" +
                "That policy may differ from ours. If you can&#8217;t find the privacy policy of any of these sites via a link from the site&#8217;s homepage, you should contact the site directly for more information.<br />\n" +
                "Any information you provide directly to a third party merchant; website or application is not covered by this privacy notice. We are not responsible for the privacy or security practices of merchants or other third parties with whom you choose to share your personal information directly. We encourage you to review the privacy policies of any third party to whom you choose to share your personal information directly.</p>\n" +
                "<p><strong>Information Security :</strong></p>\n" +
                "<p><strong>MoneyMint</strong> has security measures in place to protect the loss, misuse, and alteration of the information under our control. Whenever you change or access your account information, we offer the use of a secure server. Once your information is in our possession we adhere to strict security guidelines, protecting it against unauthorized access.</p>\n" +
                "<p><strong>Permissions that we require :</strong></p>\n" +
                "<p><strong>MoneyMint</strong> app for Android requires following permissions to perform normally on your device :</p>\n" +
                "<ul>\n" +
                "<li>Device &amp; app history – Retrieve apps that are currently running on your device</li>\n" +
                "<li>Identity – Get details about different device identifiers for your device</li>\n" +
                "<li>Read &amp; Write Contacts: We need this permission to make your signup fast &amp; seamless. We detect email id present on your phone to assist signup process. We may also add a contact in your address book for your reference, ease of communication &amp; timely message delivery</li>\n" +
                "<li>Location – Get location of your device and associate it with your MoneyMint account so as to improve delivered services get better business lead</li>\n" +
                "<li>SMS – Primary to read and Verify OTP SMS for MoneyMint signup and registration process. We may also use it for Analytics, Marketing &amp; Targeting purposes.</li>\n" +
                "<li>Phone Status -Keep app performance optimized for various statuses of your phone, such as locked / unlocked status etc.</li>\n" +
                "<li>Photos/Media/Files &amp; Storage Access- We only read and access various files stored by MoneyMint app on your device to keep the performance optimized.</li>\n" +
                "<li>Wi-Fi connection information – We read the status of Wi-Fi connection on your device to notify you about the same and also intimate you to use <strong>MoneyMint</strong> at that time so as to save on your data costs</li>\n" +
                "<li>Other Permissions such as: Receive data from Internet, full network access, prevent device from sleeping, reorder running apps, view network connections, read Google service configuration, control vibration, run at start-up, modify system settings &#8212; We use these services to give our users best possible experience on MoneyMint and improve our services from time to time.</li>\n" +
                "</ul>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
    }
}
