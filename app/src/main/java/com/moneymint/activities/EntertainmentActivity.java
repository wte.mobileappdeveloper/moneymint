package com.moneymint.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.moneymint.R;
import com.moneymint.configs.Configs;
import com.moneymint.models.EntertainModel;
import com.moneymint.recyclerAdapter.EntertainmentAdapter;
import com.moneymint.recyclerAdapter.HomeRecyclerAdapter_TodayTasks;
import com.startapp.android.publish.adsCommon.StartAppAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.moneymint.configs.Configs.KEY_CLIENT_ID;
import static com.moneymint.configs.Configs.clientId;
import static com.moneymint.utils.Constant.ENTERTAINMENT;

public class EntertainmentActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private WebView webTopNews;
    private ProgressBar webLoading;
    RecyclerView recycleEntertainment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entertainment);
        toolbar= findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("Entertainment News");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        webLoading=findViewById(R.id.webLoading);


        recycleEntertainment=findViewById(R.id.recycleEntertainment);

        getEntertainmentNews();
    }

    private void getEntertainmentNews()
    {
        webLoading.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ENTERTAINMENT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                webLoading.setVisibility(View.GONE);
                Log.e("response regestration",response);
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    ArrayList<EntertainModel> entertainModelArrayList=new ArrayList<>();
                    for (int i=0;i<jsonArray.length();i++)
                    {
                        EntertainModel entertainModel=new EntertainModel();
                        entertainModel.setTitle(jsonArray.getJSONObject(i).getJSONObject("title").getString("rendered"));
                        entertainModel.setImage(jsonArray.getJSONObject(i).getJSONObject("_embedded").getJSONArray("wp:featuredmedia").getJSONObject(0).getJSONObject("media_details").getJSONObject("sizes").getJSONObject("medium").getString("source_url"));
                        entertainModel.setLink(jsonArray.getJSONObject(i).getString("link"));
                        entertainModelArrayList.add(entertainModel);
                    }

                    EntertainmentAdapter globalAdapter= new EntertainmentAdapter(EntertainmentActivity.this,entertainModelArrayList);
                    recycleEntertainment.setAdapter(globalAdapter);
                    recycleEntertainment.setLayoutManager(new GridLayoutManager(EntertainmentActivity.this, 1));
                    recycleEntertainment.setHasFixedSize(true);
                    globalAdapter.notifyDataSetChanged();


                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                webLoading.setVisibility(View.GONE);
            }
        }) ;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }
    @Override
    public void onBackPressed() {
        StartAppAd.onBackPressed(this);
        super.onBackPressed();
    }


}
