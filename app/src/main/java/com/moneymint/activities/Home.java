package com.moneymint.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneymint.ApiHandler.VolleySingleton;
import com.moneymint.LocalSaved.PreferenceHelper;
import com.moneymint.R;
import com.moneymint.configs.Configs;
import com.moneymint.models.ListNotifyModel;
import com.moneymint.models.UserDetailsModel;
import com.moneymint.utils.InternetConnectivity.BaseActivity;
import com.moneymint.viewPager.ViewPagerAdapter;
import com.moneymint.fragments.NotificationFragment;
import com.moneymint.fragments.ProfileFragment;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static com.moneymint.R.id.tab;
import static com.moneymint.configs.Configs.KEY_CLIENT_ID;
import static com.moneymint.configs.Configs.clientId;
import static com.moneymint.utils.Constant.ANDROID_ID;
import static com.moneymint.utils.Constant.TODAY_TASK_STATUS_BANNER;
import static com.moneymint.utils.Constant.TODAY_TASK_STATUS_SPEC;

public class Home extends BaseActivity {
    int oneDp;

    TabLayout tabLayout;
    DrawerLayout drawerLayout;
    ViewPager viewPager;
    NavigationView navigationView;
    BottomNavigationView bottomNavigationView;
    Toolbar toolbar;
    TextView username;
    FrameLayout container;
    RelativeLayout homeLayout;
    Animation bottomUp, topDown,leftToRight;
    boolean doubleBackToExitPressedOnce = false;
    ImageView wallet;
    private static final String KEY_USER_ID = "customer_id";
    public static final String KEY_USERID = "customer_id";
    Dialog dialog;
    private SharedPreferences myPrefsForSesssion;
    private String  userId, gender, mobileNo;
    int anim = 0;

    final UserDetailsModel userDetailsModel=new UserDetailsModel();


    ActionBarDrawerToggle actionBarDrawerToggle;
    private ProgressDialog progressDialog;
    private String userID;
    ImageView share;
    public static TextView today_task_point;
    public static ArrayList<ListNotifyModel> al = new ArrayList<>();
    boolean bonusFlag;
    String m_androidId;
    String RefferalID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        m_androidId= Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        dialog=new Dialog(Home.this);
        oneDp = getResources().getDimensionPixelSize(R.dimen.oneDp);

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        viewPager =  findViewById(R.id.viewpager);
        tabLayout = findViewById(tab);
        navigationView = findViewById(R.id.navigation_view);
        drawerLayout = findViewById(R.id.drawer);
        container = findViewById(R.id.container);
        homeLayout = findViewById(R.id.home_layout);
        wallet = findViewById(R.id.wallet);
        share=findViewById(R.id.share_app);
        username=navigationView.getHeaderView(0).findViewById(R.id.tv_email);
        today_task_point =  findViewById(R.id.toolbar_today_task_points);


        SharedPreferences sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        userID = sharedPreferences.getString("User_Id", "");
        username.setText(sharedPreferences.getString("Name",""));

        OneSignal.setSubscription(true);
        OneSignal.sendTag("userid",userID);
        OneSignal.sendTag("name",sharedPreferences.getString("Name",""));



/////////////////////////toolbar///////////////////
        toolbar =  findViewById(R.id.toolbar_home);
        setSupportActionBar(toolbar);

        Drawable drawable= getResources().getDrawable(R.drawable.ic_menu_non_vector);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        Drawable newdrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 50, 50, true));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(newdrawable);

        navigationView.setItemIconTintList(null);



        drawerLayout.setDrawerListener(actionBarDrawerToggle);





// ----------------------------------Shared preference for login---------------------------------------


        userId = sharedPreferences.getString("User_Id", "");
        gender = sharedPreferences.getString("Gender", "");
        mobileNo= sharedPreferences.getString("Mobileno", "0000000000");



//------------------------------------navigation selection for bottom and menu---------------------------
        bottomNavigationView.setOnNavigationItemSelectedListener(bottomnavigationItemSelectedListener());
        navigationView.setNavigationItemSelectedListener(navigationItemSelectedListener());




//--------------------------------------animation/---------------------------------------------------
        bottomUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_bottom);
        topDown = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_out_top);
        leftToRight=AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.left_to_right);
        progressDialog = new ProgressDialog(this);



        userDetailsParse();

        if (getIntent().getIntExtra("focus",0)==1){

            hometabLayout(1);


        }
        else {
            hometabLayout(0);

        }
//        getCountParse();

        ////////////////////////go to my wallet page//////////////////////
        wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this, MyNewWallet.class));
                overridePendingTransition( R.anim.left_to_right_in, R.anim.left_to_right );
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareButton(Home.this);
            }
        });


        myPrefsForSesssion = getSharedPreferences("session", Context.MODE_PRIVATE);
        bonusFlag = myPrefsForSesssion.getBoolean("bonus", false);
        if (bonusFlag)
        {
            bonushDailog();

        }

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);

        getGender();
        drawerMenuAnimation();
        checkTodayBannerTaskStatus();
        checkTodaySpecialTaskStatus();
    }

    public NavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener(){


        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.about_us:
////////////////Load About us activity/////////////////////

                        startActivity(new Intent(Home.this, AboutUs.class));
                        overridePendingTransition( R.anim.left_to_right_in, R.anim.left_to_right );
                        break;

                    case R.id.faq:
                        ////////////////Load FAQ activity/////////////////////

                        startActivity(new Intent(Home.this, FAQActivity.class));
                        overridePendingTransition( R.anim.left_to_right_in, R.anim.left_to_right );
                        break;

                    case R.id.support:
                        ////////////////Load support activity/////////////////////

                        startActivity(new Intent(Home.this, Support.class));
                        overridePendingTransition( R.anim.left_to_right_in, R.anim.left_to_right );
                        break;

                    case R.id.teamdetails:

                        startActivity(new Intent(Home.this, TeamsDetails.class));
                        overridePendingTransition( R.anim.left_to_right_in, R.anim.left_to_right );
                        break;

                    case R.id.mywallet:
                        ////////////////Load Mywallet activity/////////////////////

                        startActivity(new Intent(Home.this, MyNewWallet.class));
                        overridePendingTransition( R.anim.left_to_right_in, R.anim.left_to_right );
                        break;



                    case R.id.payment_history:
                        ////////////////Load payment history/////////////////////

                        startActivity(new Intent(Home.this, Bonanza.class));
                        overridePendingTransition( R.anim.left_to_right_in, R.anim.left_to_right );
                        break;


                    case R.id.menu_exit:


                        ///////////////////logout///////////////////////
//                    logoutalertDialog(Home.this);
                        logoutalertDialog(Home.this);
                        break;
                    case R.id.mark_spam:


                        ///////////////////spam///////////////////////
                        startActivity(new Intent(Home.this, SpamActivity.class));
                        overridePendingTransition( R.anim.left_to_right_in, R.anim.left_to_right );

                        break;
                    case R.id.privacyPolicy:
                        startActivity(new Intent(Home.this, PrivacyPolicy.class));
                        overridePendingTransition( R.anim.left_to_right_in, R.anim.left_to_right );
                        break;
//
                    case R.id.menu_ern_more:


                        ///////////////////spam///////////////////////
                        startActivity(new Intent(Home.this, EShopping.class));
                        overridePendingTransition( R.anim.left_to_right_in, R.anim.left_to_right );

                        break;

                    case  R.id.menu_change_pass:
                        startActivity(new Intent(Home.this, ChangePassword.class));
                        overridePendingTransition( R.anim.left_to_right_in, R.anim.left_to_right );

                        break;


                }


                return true;
            }

        };

    }



    public BottomNavigationView.OnNavigationItemSelectedListener bottomnavigationItemSelectedListener(){
        return new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

                switch (item.getItemId()) {

                    case R.id.bottom_tab_home:
                        if (bottomNavigationView.getSelectedItemId() != R.id.bottom_tab_home) {

                            homeAnimation();
                            homeLayout.setVisibility(View.VISIBLE);
                            container.setVisibility(View.GONE);
                            hometabLayout(0);
                        }
                        else {
                            if (viewPager.getCurrentItem()!=0){
                                homeAnimation();

                                homeLayout.setVisibility(View.VISIBLE);
                                container.setVisibility(View.GONE);
                                hometabLayout(0);
                            }
                        }
                        return true;

                    case R.id.bottom_tab_notification:


////////////////Load notification Fragment/////////////////////

                        if (bottomNavigationView.getSelectedItemId() != R.id.bottom_tab_notification) {
                            animation();
                            homeLayout.setVisibility(View.GONE);
                            container.setVisibility(View.VISIBLE);
                            NotificationFragment noti_fragment = new NotificationFragment();
                            ft.replace(R.id.container, noti_fragment);
                            ft.commit();
                        }


                        return true;

                    case R.id.bottom_tab_profile:
////////////////Load profile Fragment/////////////////////
                        if (bottomNavigationView.getSelectedItemId() != R.id.bottom_tab_profile) {

                            animation();
                            homeLayout.setVisibility(View.GONE);
                            container.setVisibility(View.VISIBLE);
                            ProfileFragment profileFragment = new ProfileFragment(Home.this);
                            ft.replace(R.id.container, profileFragment);
                            ft.commit();
                        }

                        return true;



                }
                return false;
            }

        };
    }


//---------------------------------logout method-------------------------------------------------

    private void logutParse() {

        progressDialog.setMessage("Logging Out");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_logout, new Response.Listener<String>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("Logout Response",response);
                    if (jsonObject.optString("error").equals("false") || jsonObject.optString("error_msg").equals("Log Out")) {
                        Toast.makeText(Home.this,"successfully logged out", Toast.LENGTH_SHORT).show();
                        myPrefsForSesssion = getSharedPreferences("session", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = myPrefsForSesssion.edit();
                        SharedPreferences acShared= getSharedPreferences("AC_DETAILS",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor2 = acShared.edit();
                        editor2.clear().apply();

                        getSharedPreferences("REGIST_DETAILS",MODE_PRIVATE).edit().clear().apply();
                        dialog.dismiss();


                        editor.putBoolean("remember", false);
                        editor.apply();
                        editor.clear().apply();
                        //............................Reward Video Session Clean............

                        PreferenceHelper.clearAllPreferences(Home.this);

                        //.........................End......................................

                        Intent logintent = new Intent();
                        logintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finishAffinity();
                        startActivity(new Intent(Home.this, Login.class));

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Home.this,"Some error occurred", Toast.LENGTH_SHORT).show();

                progressDialog.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("customer_id", userId);
                params.put(KEY_CLIENT_ID,clientId(Home.this));
                Log.e("LogoutParams",params.toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }




    //----------------------------------------------initiate tab layout--------------------------------------

    public void hometabLayout(int focus) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),focus,Home.this);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }



    //-------------------------------------------alert dialog----------------------------------------------
    public void logoutalertDialog(final Activity activity) {

        dialog = new Dialog(activity,R.style.floatingDialog);
        dialog.setContentView(R.layout.logout_dialog);
        Button logout_yes =  dialog.findViewById(R.id.txt_yes);
        Button logout_no =  dialog.findViewById(R.id.txt_no);
        logout_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        logout_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Logout","Function calll");
                logutParse();
            }
        });
        dialog.show();

    }


    //----------------------------------animations other than home TabLayout-----------------------------
    public void animation() {


        if (anim == 0) {
            container.startAnimation(bottomUp);
            anim = 1;
        } else {
            container.startAnimation(topDown);
            anim = 0;
        }

    }


    //------------------------------------animations for home TabLayout------------------------------------
    public void homeAnimation() {
        if (anim == 0) {
            homeLayout.startAnimation(bottomUp);
            anim = 1;
        } else {
            homeLayout.startAnimation(topDown);
            anim = 0;
        }
    }


    public void userDetailsParse() {

        progressDialog.setMessage("Loading...");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_UserDetails,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("usedDe",response);

                        try {

                            JSONObject jObject = new JSONObject(response);
                            if (jObject.optString("error").equalsIgnoreCase("false") || jObject.optString("error_msg").equalsIgnoreCase("Success")) {
                                //For Get date From Server
                                String _UName = jObject.getString("Name");
                                String _Mobileno = jObject.getString("Mobileno");
                                String _EmailID = jObject.getString("EmailID");
                                String _City = jObject.getString("City");
                                String _AccountNo = jObject.getString("Accountno");
                                String _BankName = jObject.getString("BankName");
                                String _BranchAddress = jObject.getString("BranchAddress");
                                String _IFSCCode = jObject.getString("IFSCCode");
                                //For Set Date From Server In UI

                                userDetailsModel.set_UName(_UName);
                                userDetailsModel.set_Mobileno(_Mobileno);
                                userDetailsModel.set_EmailID(_EmailID);
                                userDetailsModel.set_City(_City);
                                userDetailsModel.set_AccountNo(_AccountNo);
                                userDetailsModel.set_BankName(_BankName);
                                userDetailsModel.set_BranchAddress(_BranchAddress);
                                userDetailsModel.set_IFSCCode(_IFSCCode);

                                saveAccountDetails(_AccountNo,_BankName,_BranchAddress,_IFSCCode);

                                SharedPreferences sharedpreferences = getSharedPreferences("user_name", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("uName",_UName);
                                editor.apply();


//                                txt_Uemail.setText(_EmailID);
//                                txt_UMobile.setText(_Mobileno);
//                                txt_Ucity.setText(_City);
//                                txt_UAcNumber.setText(_AccountNo);
//                                txt_UBranchName.setText(_BankName);
//                                txt_BranchAddress.setText(_BranchAddress);
//                                txt_IFSCCode.setText(_IFSCCode);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();


                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("usedDetails exception",error.toString());

                        Toast.makeText(Home.this, "Server is not responding", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_USER_ID, userID);
                params.put(KEY_CLIENT_ID, clientId(Home.this));
                Log.e("usedDetails params",params.toString());



                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    //----------------------------------------share your app popup---------------------------------------
    private void shareButton(final Activity activity){
        final AlertDialog share;

        final AlertDialog.Builder adb = new AlertDialog.Builder(activity, R.style.floatingDialog);
        View view = activity.getLayoutInflater().inflate(R.layout.fragment_share_and_earn, null);
        adb.setView(view);
        share = adb.create();
        share.getWindow().getAttributes().windowAnimations = R.style.AppTheme;

        Button sharebtn;
        sharebtn=view.findViewById(R.id.share_button);
        TextView hw_it_wrks;
        hw_it_wrks=view.findViewById(R.id.hw_it_wrks);
        TextView ref_code=view.findViewById(R.id.refferal_code);
        ref_code.setText(mobileNo);

        sharebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    appShare();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        hw_it_wrks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                howItWorksPopup(Home.this);
            }
        });
        share.show();
    }


    //---------------------------------------howItWorksPopup popup---------------------------------------
    private void howItWorksPopup(final Activity activity){
        final AlertDialog share;

        final AlertDialog.Builder adb = new AlertDialog.Builder(activity, R.style.floatingDialog);
        View view = activity.getLayoutInflater().inflate(R.layout.how_it_work_popup, null);
        adb.setView(view);
        share = adb.create();
        share.getWindow().getAttributes().windowAnimations = R.style.AppTheme;

        ImageView cancel;
        cancel=view.findViewById(R.id.hw_it_works_cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    share.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        share.show();
    }

    public void saveAccountDetails(String _acNumber,String _bankName,String _branchAddress, String _ifceCode) {
        SharedPreferences acShared= getSharedPreferences("AC_DETAILS",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = acShared.edit();
        editor.putString("AC_NUMBER",_acNumber);
        editor.putString("BANK_NAME",_bankName);
        editor.putString("BRANCH_ADDRESS",_branchAddress);
        editor.putString("IFSC_CODE",_ifceCode);
        editor.apply();

    }


    public void getGender(){

        View header = navigationView.getHeaderView(0);

        ImageView img_pro_navi =  header.findViewById(R.id.img_profile_navigation);

        if (gender.equalsIgnoreCase("F"))
        {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                img_pro_navi.setImageDrawable(getApplicationContext().getDrawable(R.drawable.girl_user));
            }
        }else
        {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                img_pro_navi.setImageDrawable(getApplicationContext().getDrawable(R.drawable.profile));
            }
        }
    }

    public void bonushDailog() {
// Create custom dialog object
        final Dialog dialog = new Dialog(Home.this);
        dialog.setContentView(R.layout.bonush_dialog);
        dialog.show();

        SharedPreferences.Editor bonusEdito=myPrefsForSesssion.edit();
        bonusEdito.putBoolean("bonus",false);
        bonusEdito.apply();


    }


    @Override
    protected void onPause() {
        super.onPause();
        progressDialog.dismiss();

    }


    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    @Override
    public void onBackPressed() {
        //Checking for fragment count on backstack
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to Exit.", Toast.LENGTH_SHORT).show();
//            clearApplicationData();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
        }
    }

    void drawerMenuAnimation(){
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {

//                ActionMenuItemView itemView = (ActionMenuItemView) navigationView.getMenu().getItem(3).getActionView();
//
//                itemView.setAnimation(leftToRight);



            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }


    public void appShare(){
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "lakshme");
            String sAux = "\nLet me recommend you this application\n"+"Refferal Code: "+userID+"\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=com.moneymint \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "choose one"));
        } catch(Exception ignored) {

        }
    }




    private void checkTodaySpecialTaskStatus()
    {

        final String m_androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_checkVideotask, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    Log.e("forgetResponse0",response);
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.optString("error").equalsIgnoreCase("true"))
                    {
                        PreferenceHelper.setStringPreference(Home.this,TODAY_TASK_STATUS_SPEC,"true");
                    }else {
                        PreferenceHelper.setStringPreference(Home.this,TODAY_TASK_STATUS_SPEC,"false");

                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(Home.this,"Some error occurred",Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_USER_ID, userID);
                params.put(KEY_CLIENT_ID, clientId(Home.this));
                params.put(ANDROID_ID,m_androidId);

                return params;
            }

        };

        VolleySingleton.getInstance(Home.this).addToRequestQueue(stringRequest);

    }


    private void checkTodayBannerTaskStatus()
    {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_checkdailtask, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    Log.e("forgetResponse0",response);
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.optString("error").equalsIgnoreCase("true"))
                    {
                        PreferenceHelper.setStringPreference(Home.this,TODAY_TASK_STATUS_BANNER,"true");
                    }else {
                        PreferenceHelper.setStringPreference(Home.this,TODAY_TASK_STATUS_BANNER,"false");

                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(Home.this,"Some error occurred",Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_USER_ID, userID);
                params.put(KEY_CLIENT_ID, clientId(Home.this));
                params.put(ANDROID_ID,m_androidId);

                return params;
            }

        };

        VolleySingleton.getInstance(Home.this).addToRequestQueue(stringRequest);

    }


}
