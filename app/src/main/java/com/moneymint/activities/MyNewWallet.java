package com.moneymint.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneymint.R;
import com.moneymint.configs.Configs;
import com.moneymint.models.ApprovedMonthlyHistoryModel;
import com.moneymint.models.RedeemableHistoryModel;
import com.moneymint.models.TransferHistoryModel;
import com.moneymint.recyclerAdapter.ApprovedMonthlyHistoryAdapter;
import com.moneymint.recyclerAdapter.RedeemableHistoryAdapter;
import com.moneymint.recyclerAdapter.TransferHistoryAdapter;
import com.moneymint.utils.InternetConnectivity.BaseActivity;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.moneymint.configs.Configs.KEY_CLIENT_ID;
import static com.moneymint.configs.Configs.clientId;

public class MyNewWallet extends BaseActivity {

    LinearLayout linear_layout_redeemable,linear_layout_earned,transfered_layout;
    RelativeLayout linear_layout_waiting;
    TextView txt_earned,txt_waiting_approval,txt_reedeemed,txt_trnsfered,txt_waiting_reward,txt_moon;
    private static final String KEY_USER_ID = "customer_id";
    private SharedPreferences preferences;
    ExpandableLinearLayout expandableMonthlyApproved,expandableRedeemableHistory,expandedTransferredHistory;
    String userID;
    RecyclerView recyclerApprovedMonthlyHistory,recycler_redeemable;
    SwipeRefreshLayout swipeRefreshLayout;

    public static String bonus;
    private RecyclerView recycler_transfer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_new_wallet);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toggle_toolbar);
        setSupportActionBar(toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("My Wallet");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        //................Set TextView
        txt_earned=findViewById(R.id.txt_earned);
        txt_waiting_approval=findViewById(R.id.txt_waiting_approval);
        txt_waiting_reward=findViewById(R.id.txt_waiting_reward);
        txt_reedeemed=findViewById(R.id.txt_reedeemed);
        txt_moon=findViewById(R.id.txt_moon);
        txt_trnsfered=findViewById(R.id.txt_trnsfered);
        swipeRefreshLayout=findViewById(R.id.swipe);
        expandableMonthlyApproved=findViewById(R.id.expanded_approved_history);
        expandableRedeemableHistory=findViewById(R.id.expanded_redeemable_history);
        expandedTransferredHistory=findViewById(R.id.expanded_transfer_history);
        recycler_transfer=findViewById(R.id.transfer_history_recycler);
        recyclerApprovedMonthlyHistory=findViewById(R.id.approvedMonthly_history_recycler);
        recycler_redeemable=findViewById(R.id.redemable_history_recycler);

        //................ Set Linear Layout

        linear_layout_earned=findViewById(R.id.linear_layout_earned);
        linear_layout_waiting=findViewById(R.id.linear_layout_waiting);
        linear_layout_redeemable=findViewById(R.id.linear_layout_redeemable);
        transfered_layout=findViewById(R.id.linear_layout_transfered);




        //Set Click Event
        preferences = getSharedPreferences("login", Context.MODE_PRIVATE);


        userID = preferences.getString("User_Id", "");
        paymentHistoryParse();
        transferHistory();
        redeemable();
        approvedMonthlyRecord();


        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.CYAN, Color.BLUE, Color.YELLOW, Color.MAGENTA);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);

                transferHistory();
                redeemable();
                approvedMonthlyRecord();
                paymentHistoryParse();
            }
        });


        linear_layout_earned.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandableMonthlyApproved.toggle();
            }
        });
        linear_layout_redeemable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                expandableRedeemableHistory.toggle();

                String month = new SimpleDateFormat("MMM-yyyy").format(new Date());
                Intent intent=new Intent(MyNewWallet.this,MonthlyTaskWiseHistory.class);
                intent.putExtra("status","R");
                intent.putExtra("month",month);
                Log.e("month",month);
                startActivity(intent);

            }
        });
        transfered_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                expandedTransferredHistory.toggle();
            }
        });

    }

    //////////////////////parse payment history //////////////////////////////
    private void paymentHistoryParse() {

        final ProgressDialog progressDialog=new ProgressDialog(MyNewWallet.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_Wallet,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        swipeRefreshLayout.setRefreshing(false);

                        try {

                            JSONObject jObject = new JSONObject(response);
                            Log.e("New Wallet payment",response);

                            if (jObject.optString("error").equalsIgnoreCase("false")) {

//                                //For Get date From Server
                                txt_waiting_approval.setText(jObject.getString("Total_Pending"));
                                txt_waiting_reward.setText(jObject.getString("Total_Moon"));
                                txt_moon.setText(jObject.getString("Total_Moon"));
                                bonus=jObject.getString("Total_approved_amt");


                            } else if (jObject.optString("error").equalsIgnoreCase("true")) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (!MyNewWallet.this.isFinishing() && progressDialog != null&& progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);
                if (!MyNewWallet.this.isFinishing() && progressDialog != null&& progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_CLIENT_ID, clientId(MyNewWallet.this));
                params.put(KEY_USER_ID, userID);
                return params;

            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


    //////////////////////approved history////////////////////////////

    private  void approvedMonthlyRecord(){


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_approved_monthlyHistory,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jObject = new JSONObject(response);
                            Log.e("New Wallet approved",response);
                            ArrayList<ApprovedMonthlyHistoryModel> approvedMonthlyHistoryModelArrayList=new ArrayList<>();

                            approvedMonthlyHistoryModelArrayList.clear();
                            if (jObject.optString("error").equalsIgnoreCase("false")) {
                                txt_earned.setText(jObject.getString("total"));

                                findViewById(R.id.approved_nodata_layout).setVisibility(View.GONE);

                                JSONArray data=jObject.getJSONArray("data");

                                for (int i=0;i<data.length();i++){
                                    JSONObject jsonObject1=data.getJSONObject(i);

                                    ApprovedMonthlyHistoryModel approvedMonthlyHistoryModel=new ApprovedMonthlyHistoryModel();

                                    approvedMonthlyHistoryModel.setIncomebychild(jsonObject1.getString("incomebychild"));
                                    approvedMonthlyHistoryModel.setMonthwisetotal(jsonObject1.getString("monthwisetotal"));
                                    approvedMonthlyHistoryModel.setMonthYear(jsonObject1.getString("MonthYear"));
                                    approvedMonthlyHistoryModel.setSelfincome(jsonObject1.getString("selfincome"));

                                    approvedMonthlyHistoryModelArrayList.add(approvedMonthlyHistoryModel);



                                }


                            } else if (jObject.optString("error").equalsIgnoreCase("true")) {
                                findViewById(R.id.approved_nodata_layout).setVisibility(View.VISIBLE);
                                txt_earned.setText("0");

                            }


                            ApprovedMonthlyHistoryAdapter approvedMonthlyHistoryAdapter =new ApprovedMonthlyHistoryAdapter(MyNewWallet.this,approvedMonthlyHistoryModelArrayList);
                            recyclerApprovedMonthlyHistory.setAdapter(approvedMonthlyHistoryAdapter);
                            recyclerApprovedMonthlyHistory.setLayoutManager(new LinearLayoutManager(MyNewWallet.this));
                            recyclerApprovedMonthlyHistory.setHasFixedSize(true);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_CLIENT_ID, clientId(MyNewWallet.this));
                params.put(KEY_USER_ID, userID);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));





    }

    //////////////////////redeemable history////////////////////////////

    private  void redeemable(){


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_redeemable_History,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("New Wallet reedemable",response);
                        try {

                            JSONObject jObject = new JSONObject(response);
                            ArrayList<RedeemableHistoryModel> redeemableHistoryModelArrayList=new ArrayList<>();

                            redeemableHistoryModelArrayList.clear();

                            if (jObject.optString("error").equalsIgnoreCase("false")) {
                                txt_reedeemed.setText(jObject.getString("total"));
                                findViewById(R.id.redeemable_nodata_layout).setVisibility(View.GONE);


                                JSONArray data=jObject.getJSONArray("data");
                                for (int i=0;i<data.length();i++){
                                    JSONObject jsonObject1=data.getJSONObject(i);

                                    RedeemableHistoryModel redeemableHistoryModel=new RedeemableHistoryModel();

                                    redeemableHistoryModel.setIncomebychild(jsonObject1.getString("incomebychild"));
                                    redeemableHistoryModel.setMonthwisetotal(jsonObject1.getString("monthwisetotal"));
                                    redeemableHistoryModel.setMonthYear(jsonObject1.getString("MonthYear"));
                                    redeemableHistoryModel.setSelfincome(jsonObject1.getString("selfincome"));
                                    redeemableHistoryModel.setRedeemdate(jsonObject1.getString("redeemeddate"));

                                    redeemableHistoryModelArrayList.add(redeemableHistoryModel);



                                }



                            } else if (jObject.optString("error").equalsIgnoreCase("true")) {
                                findViewById(R.id.redeemable_nodata_layout).setVisibility(View.VISIBLE);
                                txt_reedeemed.setText("0");

                            }
                            RedeemableHistoryAdapter redeemableHistoryAdapter=new RedeemableHistoryAdapter(MyNewWallet.this,redeemableHistoryModelArrayList);
                            recycler_redeemable.setAdapter(redeemableHistoryAdapter);
                            recycler_redeemable.setLayoutManager(new LinearLayoutManager(MyNewWallet.this));
                            recycler_redeemable.setHasFixedSize(true);


                        } catch (JSONException e) {


                            e.printStackTrace();
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("monthwise redemerr",error.toString());

                swipeRefreshLayout.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_CLIENT_ID, clientId(MyNewWallet.this));
                params.put(KEY_USER_ID, userID);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    //////////////////////transfer history////////////////////////////

    private  void transferHistory(){





        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_tansfer_History,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("New_Wallet_transfer",response);

                        try {

                            JSONObject jObject = new JSONObject(response);
                            ArrayList<TransferHistoryModel> transferHistoryModelArrayList=new ArrayList<>();

                            transferHistoryModelArrayList.clear();

                            if (jObject.optString("error").equalsIgnoreCase("false")) {

                                findViewById(R.id.trnsfer_nodata_layout).setVisibility(View.GONE);

                                txt_trnsfered.setText(jObject.getString("total"));

                                JSONArray data=jObject.getJSONArray("data");
                                for (int i=0;i<data.length();i++){
                                    JSONObject jsonObject1=data.getJSONObject(i);

                                    TransferHistoryModel transferHistoryModel=new TransferHistoryModel();

//                                    transferHistoryModel.setBalance(jsonObject1.getString("balance"));
                                    transferHistoryModel.setEntrydate(jsonObject1.getString("entrydate"));
                                    transferHistoryModel.setPaid_amt(jsonObject1.getString("transferamt"));
                                    transferHistoryModel.setTds(jsonObject1.getString("transnsfer_amt_tds"));
                                    transferHistoryModel.setAdmnChrge(jsonObject1.getString("transnsfer_amt_admch"));

                                    transferHistoryModelArrayList.add(transferHistoryModel);



                                }



                            } else if (jObject.optString("error").equalsIgnoreCase("true")) {
                                findViewById(R.id.trnsfer_nodata_layout).setVisibility(View.VISIBLE);
                                txt_trnsfered.setText("0");

                            }
                            TransferHistoryAdapter redeemableHistoryAdapter=new TransferHistoryAdapter(MyNewWallet.this,transferHistoryModelArrayList);
                            recycler_transfer.setAdapter(redeemableHistoryAdapter);
                            recycler_transfer.setLayoutManager(new LinearLayoutManager(MyNewWallet.this));
                            recycler_transfer.setHasFixedSize(true);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_CLIENT_ID, clientId(MyNewWallet.this));
                params.put(KEY_USER_ID, userID);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition( R.anim.ritht_to_left_in, R.anim.ritht_to_left );

    }
}
