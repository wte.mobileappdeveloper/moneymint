package com.moneymint.activities;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.moneymint.R;
import com.moneymint.utils.InternetConnectivity.BaseActivity;
///////////////////////show about us webpage of bluebuck/////////////////

public class AboutUs extends BaseActivity {
    private Toolbar toolbar;
    private WebView about_Webview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        toolbar= (Toolbar) findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("About Us");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        about_Webview = (WebView) findViewById(R.id.about_Webview);
        about_Webview.loadUrl("http://www.fdelight.co.in/privacy-policy-moneymint/");

        WebSettings webSettings = about_Webview.getSettings();
        webSettings.setJavaScriptEnabled(true);


        final ProgressDialog pd = ProgressDialog.show(AboutUs.this, "", "Please wait...", true);
        pd.setCancelable(true);
        about_Webview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                pd.show();
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                pd.dismiss();


            }

        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition( R.anim.ritht_to_left_in, R.anim.ritht_to_left );

    }
}
