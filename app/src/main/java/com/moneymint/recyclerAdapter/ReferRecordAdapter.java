package com.moneymint.recyclerAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moneymint.R;
import com.moneymint.models.ReferReportModel;

import java.util.ArrayList;

/**
 * Created by lalji on 28/2/18.
 */

public class ReferRecordAdapter extends RecyclerView.Adapter<ReferRecordAdapter.ViewHolder> {

    Context context;
    ArrayList<ReferReportModel> referList;


    public ReferRecordAdapter(Context mcontext,ArrayList<ReferReportModel> mreferList)
    {
        this.context=mcontext;
        this.referList=mreferList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.refer_row_layout,parent,false);
        return new ReferRecordAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {

        final ReferReportModel postion =referList.get(i);

        holder.txt_refer_designation.setText(postion.getTaskName());
        holder.txt_refer_mname.setText(postion.getMemberName());
        holder.txt_refer_points.setText(postion.getMeberPoints());


    }

    @Override
    public int getItemCount() {

        return referList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_refer_mname,txt_refer_designation,txt_refer_points;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_refer_mname=itemView.findViewById(R.id.txt_refer_mname);
            txt_refer_designation=itemView.findViewById(R.id.txt_refer_designation);
            txt_refer_points=itemView.findViewById(R.id.txt_refer_points);

            txt_refer_designation.setSelected(true);
            txt_refer_mname.setSelected(true);
            txt_refer_points.setSelected(true);
        }
    }
}
