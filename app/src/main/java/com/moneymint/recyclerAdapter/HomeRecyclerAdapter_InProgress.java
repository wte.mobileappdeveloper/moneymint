package com.moneymint.recyclerAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moneymint.R;
import com.moneymint.activities.EarnOffers;
import com.moneymint.fragments.Frag_InProgress;
import com.moneymint.models.TodayTaskModel;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by android on 26/12/17.
 */

public class HomeRecyclerAdapter_InProgress extends RecyclerView.Adapter<HomeRecyclerAdapter_InProgress.Holder> {

    private ArrayList<TodayTaskModel> list;
    private Context context;
    Date init_time,current_time;

    public HomeRecyclerAdapter_InProgress(Context context,ArrayList<TodayTaskModel> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_inprogress_task,parent,false);

        return new HomeRecyclerAdapter_InProgress.Holder(view);

    }




    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(Holder holder, int i) {


///////////////////////////set random colors//////////////////////////

        List<String> colors;

        colors=new ArrayList<String>();
        colors.clear();

        colors.add("#66BB6A");
        colors.add("#359FF3");
        colors.add("#F87C2C");





        final TodayTaskModel position = list.get(i);
        holder.txt_Amz.setText(position.getPackage_Name());
        holder.str_txt_Amz = position.getPackage_Name();
        holder.work_url = position.getWork_url();
        holder.package_id = position.getPackage_id();
        holder.url_webView = position.getWebUrl();

//        holder.txt_FullDesc.setText(position.getAppDesc());
        holder.txt_subDesc.setText(position.getAppSdesc());
        holder.txt_amt_crdt.setText(position.getAmount_Credit());
        holder.str_txt_credit_amt=position.getAmount_Credit();
        holder.url_icons = position.getIcon();
        holder.full_desc=position.getAppDesc();
        holder.workStatus=position.getWorkStatus();
        holder.button_text = position.getPackage_Name();
        holder.ClickBLabel=position.getClickBLabel();
        holder.short_desc=position.getAppSdesc();
        holder.pause=position.getPause();




        holder.full_desc_hindi=position.getAppDesc_hindi();
        holder.short_desc_hindi=position.getAppSdesc_hindi();
        holder.work_url_hindi=position.getWebUrl_hindi();





//        if (position.getTaskDuration().equalsIgnoreCase("Y"))
//        {
//            vibrateNotification();
//            holder.warningImage.setVisibility(View.VISIBLE);
////            Glide.with(context).asGif()
////                    .load(R.drawable.warning_triangle)
////                    .into(holder.warningImage);
////            Picasso.with(context)
////                    .load(R.drawable.warning_triangle)
////                    .into(holder.warningImage);
//        }



//        if (timeDifference(holder.package_id)>=150){
//
//
//            Glide.with(context).asGif()
//                    .load(R.drawable.giphy2)
//                    .into(holder.warningImage);
//        }





        try {
            Random r = new Random();
            int i1 = r.nextInt(3- 0) + 0;
            GradientDrawable draw = new GradientDrawable();
            draw.setColor(Color.parseColor(colors.get(i1)));
            draw.setCornerRadius(3);
            holder.executeTask.setBackground(draw);
        }catch (Exception e){

        }



        if (holder.url_icons.isEmpty()) { //url.isEmpty()
            Picasso.with(context)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.image);

        } else {
            Picasso.with(context)
                    .load(list.get(i).getIcon())
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.image); //this is your ImageView
        }




    }

    @Override
    public int getItemCount() {
        return list.size();
    }



    public class Holder extends RecyclerView.ViewHolder {
        ImageView image;
        LinearLayout executeTask;

        ImageView starImageView;
        TextView txt_Amz, txt_subDesc, txt_FullDesc, txt_Rate, txt_amt_crdt,txt_complet;
        String str_txt_Amz, pause, package_id;
        String full_desc_hindi;
        String short_desc_hindi;
        String work_url_hindi;
        String work_url,str_txt_credit_amt,full_desc;
        String url_icons;
        String url_webView;
        String workStatus;
        Button btn_install;
        public String button_text;
        public String ClickBLabel;
        CardView card;
        ImageView warningImage;
        public String short_desc;

        public Holder(View itemView) {
            super(itemView);
            executeTask=itemView.findViewById(R.id.executetask_home_recycler);
            image=itemView.findViewById(R.id.img_logo);
            context = itemView.getContext();
            starImageView = (ImageView) itemView.findViewById(R.id.img_star);
            txt_Amz = (TextView) itemView.findViewById(R.id.txt_title);
            txt_complet=itemView.findViewById(R.id.txt_complet);
            txt_subDesc = (TextView) itemView.findViewById(R.id.txt_sub_text);
            txt_FullDesc = (TextView) itemView.findViewById(R.id.txt_FullText);
            txt_amt_crdt = (TextView) itemView.findViewById(R.id.txt_Credit);
            card=itemView.findViewById(R.id.card_view);
            warningImage = (ImageView) itemView.findViewById(R.id.warning);


            warningImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    warningPopup((Activity) context);
                }
            });

            txt_complet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    Intent intent = new Intent(context, EarnOffers.class);

                    //  Toast.makeText(v.getContext(),"Open New Fragment",Toast.LENGTH_LONG).show();

                    bundle.putString("txt_Amz", button_text);
                    bundle.putString("url_webView", url_webView);
                    bundle.putString("package_id", package_id);
                    bundle.putString("work_url", work_url);
                    bundle.putString("icon", url_icons);
                    bundle.putString("txt_Credit",str_txt_credit_amt);
                    bundle.putString("full_desc",full_desc);
                    bundle.putString("short_desc",short_desc);
                    bundle.putString("full_desc_hindi",full_desc_hindi);
                    bundle.putString("short_desc_hindi",short_desc_hindi);
                    bundle.putString("work_url_hindi",work_url_hindi);
                    bundle.putString("ClickBLabel","Complete Task");
                    bundle.putString("pause",pause);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            });
        }



    }
    private void vibrateNotification()
    {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent = new Intent(context, Frag_InProgress.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.moneymint);
        String message="Reasons for your task In Progress";
        String title="Task In Progress !!!";
        int icon = R.drawable.moneymint;
        long when = System.currentTimeMillis();
        Notification notification = new Notification(icon, title, when);
        notification = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(intent)
                .setSmallIcon(R.drawable.moneymint)
                .setColorized(true)
                .setWhen(when)
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap).setSummaryText(message))
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

// Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);

    }


    private long timeDifference(String package_id){

        SharedPreferences sharedPreferences = context.getSharedPreferences("campaignInitiateTime", Context.MODE_PRIVATE);


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:mm:dd:hh:mm aa");
        String currentDate_Time = sdf.format(new Date());

        try {
            init_time=sdf.parse(sharedPreferences.getString(package_id,"3000:00:00:00:00 am"));
            current_time=sdf.parse(currentDate_Time);
        } catch (ParseException e) {

        }


        long mills=current_time.getTime()-init_time.getTime();
        long hours = (mills/(1000 * 60 * 60));
        long mins = (mills/(1000*60)) % 60;
        long totalmin=mins + (hours * 60);

        Log.e("time",String.valueOf(totalmin));

        return totalmin;
    }

    private void warningPopup(final Activity activity){
        final AlertDialog share;

        final AlertDialog.Builder adb = new AlertDialog.Builder(activity, R.style.floatingDialog);
        View view = activity.getLayoutInflater().inflate(R.layout.how_it_work_popup, null);
        adb.setView(view);
        share = adb.create();
        share.getWindow().getAttributes().windowAnimations = R.style.AppTheme;

        ImageView cancel;
        cancel=view.findViewById(R.id.hw_it_works_cancel);
        LinearLayout hw_it_wrks;
        hw_it_wrks=view.findViewById(R.id.how_it_work_popup);
        hw_it_wrks.setVisibility(View.GONE);
        view.findViewById(R.id.task_warning_popup).setVisibility(View.VISIBLE);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    share.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        share.show();
    }



}
