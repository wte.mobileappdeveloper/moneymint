package com.moneymint.recyclerAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moneymint.R;
import com.moneymint.models.TodayTaskModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class HomeRecyclerAdapter_completedTask extends RecyclerView.Adapter<HomeRecyclerAdapter_completedTask.ViewHolder> {

    private ArrayList<TodayTaskModel> listCompleteTask;
    private Context context;

    public HomeRecyclerAdapter_completedTask(Context mycontext, ArrayList<TodayTaskModel> listComplete)
    {
        this.listCompleteTask=listComplete;
        this.context=mycontext;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_completed_task,parent,false);
        return new HomeRecyclerAdapter_completedTask.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        final TodayTaskModel position = listCompleteTask.get(i);
        holder.txt_Amz.setText(position.getPackage_Name());
        holder.str_txt_Amz = position.getPackage_Name();
        holder.work_url = position.getWork_url();
        holder.package_id = position.getPackage_id();
        holder.url_webView = position.getWebUrl();
//        holder.txt_FullDesc.setText(position.getAppDesc());
//        holder.txt_subDesc.setText(position.getAppSdesc());
        holder.txt_amt_crdt.setText(position.getAmount_Credit());
        holder.str_txt_credit_amt=position.getAmount_Credit();
        holder.url_icons = position.getIcon();
        holder.txt_completed.setText("Completed");

      //  holder.show_point=position.getShowPoint();
        // Toast.makeText(context,holder.show_point,Toast.LENGTH_LONG).show();
        // Log.i("Show Points",holder.show_point.toString());
//        holder.showint=Integer.parseInt(holder.show_point);
//        switch ( holder.showint)
//        {
//            case 0:
//                holder.layoutStatus.setVisibility(View.INVISIBLE);
//                break;
//            case 1:
//                holder.layoutStatus.setVisibility(View.VISIBLE);
//                break;
//            default:
//                holder.layoutStatus.setVisibility(View.VISIBLE);
//        }




        if (holder.url_icons.isEmpty()) { //url.isEmpty()
            Picasso.with(context)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.imageLogo);

        } else {
            Picasso.with(context)
                    .load(listCompleteTask.get(i).getIcon())
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.imageLogo); //this is your ImageView
        }
    }

    @Override
    public int getItemCount() {
        return listCompleteTask.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView imageLogo;
        ImageView starImageView;
        TextView txt_Amz, txt_subDesc, txt_FullDesc, txt_Rate, txt_amt_crdt;
        String str_txt_Amz, user_id, package_id;

        LinearLayout layoutStatus;

        String work_url,str_txt_credit_amt;
        String url_icons;
        String url_webView;
        TextView txt_completed;

        public ViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();
            imageLogo = (ImageView) itemView.findViewById(R.id.img_logo);
            starImageView = (ImageView) itemView.findViewById(R.id.img_star);
            txt_Amz = (TextView) itemView.findViewById(R.id.txt_title);
            txt_subDesc = (TextView) itemView.findViewById(R.id.txt_sub_text);
            txt_FullDesc = (TextView) itemView.findViewById(R.id.txt_FullText);
            txt_amt_crdt = (TextView) itemView.findViewById(R.id.txt_Credit);
            layoutStatus=(LinearLayout)itemView.findViewById(R.id.layout_status);
            txt_completed = (TextView) itemView.findViewById(R.id.txt_complet);
        }
    }
}
