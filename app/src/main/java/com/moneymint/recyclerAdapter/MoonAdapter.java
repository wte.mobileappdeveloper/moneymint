package com.moneymint.recyclerAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moneymint.R;
import com.moneymint.models.MoonModel;

import java.util.ArrayList;

public class MoonAdapter extends RecyclerView.Adapter<MoonAdapter.ViewHolder>{

    private Context context;
    private ArrayList<MoonModel> moonModelArrayList;

    public MoonAdapter(Context context, ArrayList<MoonModel> arrayList)
    {
        this.context=context;
        this.moonModelArrayList=arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.self_row_layout,viewGroup,false);
        return new MoonAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {

        holder.packageName.setText(moonModelArrayList.get(i).getMoon());
        holder.date.setText(moonModelArrayList.get(i).getDate());
        holder.points.setText(moonModelArrayList.get(i).getRate());
    }

    @Override
    public int getItemCount() {
        return moonModelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView points,packageName,date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            packageName=itemView.findViewById(R.id.txt_packageName);
            points=itemView.findViewById(R.id.txt_point);
            date=itemView.findViewById(R.id.txt_date);
        }
    }
}
