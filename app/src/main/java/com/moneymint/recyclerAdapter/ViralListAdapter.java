package com.moneymint.recyclerAdapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.moneymint.R;
import com.moneymint.activities.ViralNewsActivity;
import com.moneymint.models.EntertainModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by android on 26/12/17.
 */

public class ViralListAdapter extends RecyclerView.Adapter<ViralListAdapter.Holder> {


    private ArrayList<EntertainModel> entertainModelArrayList;
    private Context context;


    public ViralListAdapter(Context context, ArrayList<EntertainModel> entertainModelArrayList){
        this.entertainModelArrayList=entertainModelArrayList;
        this.context=context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.entertain_raw,parent,false);
        return new ViralListAdapter.Holder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        holder.txtNews.setText(entertainModelArrayList.get(position).getTitle());
        if (entertainModelArrayList.get(position).getImage().isEmpty()) { //url.isEmpty()
            Picasso.with(context)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.image);

        } else {
            Picasso.with(context)
                    .load(entertainModelArrayList.get(position).getImage())
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.image); //this is your ImageView
        }

    }

    @Override
    public int getItemCount() {
        return entertainModelArrayList.size();
    }



    public class Holder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView txtNews;
        RelativeLayout rytLayout;

        public Holder(final View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.img);
            txtNews=itemView.findViewById(R.id.txtNews);
            rytLayout=itemView.findViewById(R.id.rytLayout);
            rytLayout.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View view) {

                    Bundle bundle = new Bundle();
                    Intent intent = new Intent(context, ViralNewsActivity.class);
                    intent.putExtras(bundle);
                    intent.putExtra("link",entertainModelArrayList.get(getAdapterPosition()).getLink());
                    context.startActivity(intent);

                }
            });
        }
    }}
