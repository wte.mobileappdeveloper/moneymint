package com.moneymint.recyclerAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.moneymint.R;
import com.moneymint.activities.SpamActivity;
import com.moneymint.models.SpamListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashSet;

import static com.moneymint.activities.SpamActivity.checkedStatus;

/**
 * Created by Er LaljiYadav on 6/29/2017.
 */

public class SpamAdapter extends RecyclerView.Adapter<SpamAdapter.ViewHolder> {
    public static ArrayList<SpamListModel> list;
    private ArrayList<SpamListModel> checkedList = new ArrayList<>();
    private Context mcontext;
    public HashSet<Integer> hashset;


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spam_list_layout, parent, false);
        return new SpamAdapter.ViewHolder(view);

    }

    public SpamAdapter(Context context, ArrayList<SpamListModel> list) {
        this.list = list;
        this.mcontext = context;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        final SpamListModel position = list.get(i);
        holder.spam_text.setText(position.getPackage_Name());
        holder.strChecked = position.getType().toString();
        holder.packageID = position.getType().toString();
        String spm_img_string=position.getIcon();
        // holder.chk_spam_mark.setChecked(true);

        if (spm_img_string.isEmpty()) { //url.isEmpty()
            Picasso.with(mcontext)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.spam_img);

        } else {
            Picasso.with(mcontext)
                    .load(spm_img_string)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.spam_img); //this is your ImageView
        }

        if (holder.strChecked.equalsIgnoreCase("true")) {
            holder.chk_spam_mark.setChecked(true);
        } else {
            holder.chk_spam_mark.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView spam_img;
        TextView spam_text;
        CheckBox chk_spam_mark;
        String strChecked;
        String packageID;

        public ViewHolder(View itemView) {
            super(itemView);
            spam_img = (ImageView) itemView.findViewById(R.id.spam_img);
            spam_text = (TextView) itemView.findViewById(R.id.spam_name);
            chk_spam_mark = (CheckBox) itemView.findViewById(R.id.spam_check);
            chk_spam_mark.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            SpamListModel spamListModel = new SpamListModel();
            if (chk_spam_mark.isChecked()) {

                checkedList.add(list.get(getAdapterPosition()));
//                Toast.makeText(mcontext, "Marked", Toast.LENGTH_SHORT).show();

                ((SpamActivity) mcontext).addPackageId(checkedList);
                checkedStatus++;


            } else {

                //hashset.remove(getAdapterPosition());
                checkedList.remove(list.get(getAdapterPosition()));
//                Toast.makeText(mcontext, "Unmarked", Toast.LENGTH_SHORT).show();
                ((SpamActivity) mcontext).addPackageId(checkedList);
                checkedStatus--;

            }

        }


    }

    public ArrayList<SpamListModel> getPackage_ID() {
        return checkedList;
    }

    public void setData(ArrayList<SpamListModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }
}
