package com.moneymint.recyclerAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moneymint.R;

import java.util.ArrayList;

/**
 * Created by android on 30/12/17.
 */

public class BonanzaAdapter extends RecyclerView.Adapter<BonanzaAdapter.Holder> {


    Context context;
    ArrayList bonanza=new ArrayList();
    public BonanzaAdapter(Context context){

        this.context=context;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_bonanza_history,parent,false);

        bonanza.add("10 stars in one month");
        bonanza.add("250 points in 4 days");
        bonanza.add("2 stars in one day");
        bonanza.add("20 points in one hour");
        return new BonanzaAdapter.Holder(view);

    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {




        holder.sNo.setText(String.valueOf(position+1));

        holder.bonanza.setText(bonanza.get(position).toString());




    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView bonanza,sNo,status;

        String author,title,description,url,urlToImage,publishedAt;
        public Holder(View itemView) {
            super(itemView);
            bonanza=itemView.findViewById(R.id.txt_bonanzaName);
            sNo=itemView.findViewById(R.id.txt_s_no);
            status=itemView.findViewById(R.id.txt_bonanza_status);


        }
    }
}
