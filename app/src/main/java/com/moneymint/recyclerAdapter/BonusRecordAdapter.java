package com.moneymint.recyclerAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moneymint.R;
import com.moneymint.models.SelfRecodModel;

import java.util.ArrayList;

/**
 * Created by lalji on 28/2/18.
 */

public class BonusRecordAdapter extends RecyclerView.Adapter<BonusRecordAdapter.ViewHolder> {

    Context context;
    ArrayList<SelfRecodModel> selfReportList;


    public BonusRecordAdapter(Context mcontext, ArrayList<SelfRecodModel> mselfList)
    {
        this.context=mcontext;
        this.selfReportList=mselfList;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.self_row_layout,parent,false);
        return new BonusRecordAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {

        final SelfRecodModel position=selfReportList.get(i);
        holder.packageName.setText(position.getSelfTasks());
        holder.date.setText(position.getApprovDate());
        holder.points.setText(position.getApprovPoints());
        Log.e("points recycler",position.getApprovPoints());

    }

    @Override
    public int getItemCount() {
        return selfReportList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView points,packageName,date;
        public ViewHolder(View itemView) {
            super(itemView);

            packageName=itemView.findViewById(R.id.txt_packageName);
            points=itemView.findViewById(R.id.txt_point);
            date=itemView.findViewById(R.id.txt_date);
        }
    }
}
