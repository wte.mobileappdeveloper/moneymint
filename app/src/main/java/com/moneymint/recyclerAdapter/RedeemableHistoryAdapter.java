package com.moneymint.recyclerAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moneymint.R;
import com.moneymint.activities.MonthlyTaskWiseHistory;
import com.moneymint.models.RedeemableHistoryModel;

import java.util.ArrayList;

public class RedeemableHistoryAdapter extends RecyclerView.Adapter<RedeemableHistoryAdapter.Holder> {


    Context context;
    ArrayList month=new ArrayList();
    ArrayList transfer_month=new ArrayList();
    ArrayList<RedeemableHistoryModel> redeemableHistoryModelArrayList=new ArrayList<>();
    public RedeemableHistoryAdapter(Context context, ArrayList<RedeemableHistoryModel> redeemableHistoryModelArrayList){

        this.redeemableHistoryModelArrayList=redeemableHistoryModelArrayList;
        this.context=context;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_redeemable_history,parent,false);


        return new RedeemableHistoryAdapter.Holder(view);

    }

    @Override
    public void onBindViewHolder(Holder holder, int i) {


        final RedeemableHistoryModel position = redeemableHistoryModelArrayList.get(i);



        holder.month.setText(position.getMonthYear());

        holder.transferMonth.setText(position.getRedeemdate().substring(3));

        holder.total.setText(position.getMonthwisetotal());



    }

    @Override
    public int getItemCount() {
        return redeemableHistoryModelArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView month,total,transferMonth;

        public Holder(View itemView) {
            super(itemView);
            month=itemView.findViewById(R.id.txt_month);
            total=itemView.findViewById(R.id.txt_redeemable_icome_month);
            transferMonth=itemView.findViewById(R.id.txt_transfer_month);
            month.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, MonthlyTaskWiseHistory.class);
                    intent.putExtra("month",month.getText().toString());
                    intent.putExtra("status","R");
                    context.startActivity(intent);
                }
            });


        }
    }
}
