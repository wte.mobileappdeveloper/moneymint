package com.moneymint.recyclerAdapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moneymint.R;
import com.moneymint.models.TransferHistoryModel;

import java.util.ArrayList;

public class TransferHistoryAdapter extends RecyclerView.Adapter<TransferHistoryAdapter.Holder> {


    Context context;
    ArrayList month=new ArrayList();
    ArrayList transfer_month=new ArrayList();
    ArrayList<TransferHistoryModel> transfereHistoryModelArrayList=new ArrayList<>();
    public TransferHistoryAdapter(Context context, ArrayList<TransferHistoryModel> transfereHistoryModelArrayList){

        this.transfereHistoryModelArrayList=transfereHistoryModelArrayList;
        this.context=context;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_transfer_amount,parent,false);


        return new TransferHistoryAdapter.Holder(view);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(Holder holder, int i) {


        final TransferHistoryModel position = transfereHistoryModelArrayList.get(i);



        holder.month.setText(position.getEntrydate());
        holder.transferMonth.setText(position.getPaid_amt());
        holder.txt_transfer_tds.setText(
                "-"+ Float.parseFloat(position.getTds())
                +"\n"+
                "-"+ Float.parseFloat(position.getAdmnChrge())
        );

//        holder.total.setText(position.getBalance());



    }

    @Override
    public int getItemCount() {
        return transfereHistoryModelArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView month,total,transferMonth,txt_transfer_tds;

        public Holder(View itemView) {
            super(itemView);
            month=itemView.findViewById(R.id.txt_month);
            total=itemView.findViewById(R.id.txt_redeemable_icome_month);
            txt_transfer_tds=itemView.findViewById(R.id.txt_transfer_tds);
            transferMonth=itemView.findViewById(R.id.txt_transfer_month);


        }
    }
}
