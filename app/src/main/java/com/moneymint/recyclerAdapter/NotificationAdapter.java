package com.moneymint.recyclerAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moneymint.R;
import com.moneymint.models.ListNotifyModel;

import java.util.ArrayList;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private ArrayList<ListNotifyModel> listNotifyModels;
    private Context context;

    public NotificationAdapter(Context mycontext, ArrayList<ListNotifyModel> listNotifyModels)
    {
        this.listNotifyModels=listNotifyModels;
        this.context=mycontext;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row,parent,false);
        return new NotificationAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        final ListNotifyModel position = listNotifyModels.get(i);

        holder.notifyMsg.setText(position.getNotifyMsg());
        holder.notifyDate.setText(position.getNotifyDate());
        Log.e("notif "+i,String.valueOf(position.getNotifyMsg()));

    }

    @Override
    public int getItemCount() {
        return listNotifyModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView notifyMsg;
        TextView notifyDate;

        public ViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();
            notifyMsg = (TextView) itemView.findViewById(R.id.notifymsg);
            notifyDate = (TextView) itemView.findViewById(R.id.notify_Date);
        }
    }
}
