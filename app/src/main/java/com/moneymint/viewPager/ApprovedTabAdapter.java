package com.moneymint.viewPager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.moneymint.fragments.MoonFragment;
import com.moneymint.fragments.ReferFragment;
import com.moneymint.fragments.SelfHistory;

/**
 * Created by lalji on 21/2/18.
 */

public class ApprovedTabAdapter extends FragmentStatePagerAdapter {

    int tabCount;
    //Constructor to the class
    public ApprovedTabAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }
    public ApprovedTabAdapter(FragmentManager fm) {
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = new SelfHistory();
                return fragment;

            case 1:
                fragment = new ReferFragment();
                return fragment;
            case 2:

                fragment = new MoonFragment();
                return fragment;
////            case 3:
//                fragment=new BonanzaFragment();
//                return fragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
