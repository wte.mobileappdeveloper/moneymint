package com.moneymint.viewPager;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.moneymint.fragments.Frag_todaysTask;
import com.moneymint.fragments.Frag_CompletedTask;
import com.moneymint.fragments.Frag_InProgress;

import java.util.ArrayList;

/**
 * Created by android on 26/12/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> fragment = new ArrayList<Fragment>();
    private ArrayList<String> tab = new ArrayList<>();
    private int focus;
    public ViewPagerAdapter(FragmentManager fm, int focus, Activity activity) {
        super(fm);
        fragment.add(new Frag_todaysTask(focus,activity));
        fragment.add(new Frag_InProgress(activity));
        fragment.add(new Frag_CompletedTask(activity));
        tab.add("Today's Tasks");
        tab.add("In Progress");
        tab.add("Completed");
        this.focus=focus;

    }

    @Override
    public Fragment getItem(int position) {

        return fragment.get(position);
      }

    @Override
    public int getCount() {
        return fragment.size();

    }
    @Override
    public CharSequence getPageTitle(int position) {
        return tab.get(position);
    }
}
