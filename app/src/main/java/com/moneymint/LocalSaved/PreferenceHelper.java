package com.moneymint.LocalSaved;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferenceHelper {


    public static final String PREF_NAME = PreferenceHelper.class.getName();
    public static final String PERMANENT_PREF_NAME = PreferenceHelper.class.getName() + "remember";
    private static SharedPreferences preferences;

    public static void setStringPreference(Context context, String KEY,
                                           String value) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putString(KEY, "" + value);
        editor.apply();
    }
    public static void setIntPreference(Context context, String KEY,
                                           Integer value) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putInt(KEY,  +value);
        editor.apply();
    }

    public static void isSession(Context context, String KEY,
                                 boolean value) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putBoolean(KEY, value);
        editor.apply();
    }



    public static void isFlag(Context context, String KEY,
                              boolean value) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putBoolean(KEY, value);
        editor.apply();
    }

    public static boolean isGetFlag(Context context, String KEY) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return preferences.getBoolean(KEY, false);
    }

    public static boolean isGetSession(Context context, String KEY) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return preferences.getBoolean(KEY, false);
    }
    public static String getStringPreference(Context context, String KEY) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return preferences.getString(KEY, "0");
    }
    public static Integer getIntPreference(Context context, String KEY) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return (Integer) preferences.getInt(KEY, 0);
    }


    public static void clearAllPreferences(Context context) {
        preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);

        preferences.edit().clear().apply();

    }

    public static void setPermanentStringPreference(Context context, String KEY,
                                                    String value) {
        preferences = context.getSharedPreferences(PERMANENT_PREF_NAME,
                Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putString(KEY, "" + value);
        editor.apply();
    }

    public static String getPermanentStringPreference(Context context, String KEY) {
        preferences = context.getSharedPreferences(PERMANENT_PREF_NAME,
                Context.MODE_PRIVATE);
        return preferences.getString(KEY, "");
    }

    public static void removeToKey(Context context,String KEY)
    {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        preferences.edit().remove(KEY).apply();
    }

}