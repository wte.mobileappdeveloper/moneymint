package com.moneymint.utils.notifications;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.moneymint.R;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationDisplayedResult;
import com.onesignal.OSNotificationReceivedResult;

import java.math.BigInteger;

public class MyNotificationExtenderService extends NotificationExtenderService {
    @Override
    protected boolean onNotificationProcessing(final OSNotificationReceivedResult receivedResult) {
        OverrideSettings overrideSettings = new OverrideSettings();
        overrideSettings.extender = new NotificationCompat.Extender() {
            @Override
            public NotificationCompat.Builder extend(NotificationCompat.Builder builder) {
                Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.moneymint);
                builder.setLargeIcon(icon).setSmallIcon(R.drawable.moneymint).setColorized(true);


//



                return builder.setColor(new BigInteger("009fe3", 16).intValue());
            }
        };
        try {
            Log.e("receivedResult",receivedResult.toString());
            Log.e("receivedResultbody",receivedResult.payload.body);
            Log.e("receivedResulttitle",receivedResult.payload.title);
            saveNotification(receivedResult.payload.body);
                }catch (Exception e){
                    Log.e("NOTIF_EXCEP",e.toString());
                }
        OSNotificationDisplayedResult displayedResult = displayNotification(overrideSettings);
        Log.e("OneSignalExample", "Notification displayed with id: " + displayedResult.androidNotificationId);

        return true;
    }


    public void saveNotification(String notification) {

            new DatabaseNotification(getApplicationContext()).saveData(notification);


    }

}