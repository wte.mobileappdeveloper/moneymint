package com.moneymint.utils.notifications;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatabaseNotification extends SQLiteOpenHelper {
    private SQLiteDatabase writeData,readData;

    public DatabaseNotification(Context context) {
        super(context, "Notification.db ", null, 1);
        this.writeData=getWritableDatabase();
        this.readData=getReadableDatabase();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table notification(id integer NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "notification varchar(500)," +
                "date varchar(500))");

        Log.e("Create","Table Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void saveData(String notification){
        ContentValues con=new ContentValues();

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String date = df.format(c);


        con.put("notification",notification);
        con.put("date",date);

        writeData.insert("notification",null,con);
        Log.e("Data","Data Saved");
    }


    public Cursor selectAllData() {
        return readData.rawQuery("select notification,date from notification",null);
    }

    public void deleteAllNotification(){
        Log.e("data","cleared");
        writeData.delete("notification", null, null);

    }


}
