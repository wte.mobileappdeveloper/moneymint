package com.moneymint.utils.InternetConnectivity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


public class ConnectivityReceiver extends BroadcastReceiver {

    public static int connected;


    android.support.v7.app.AlertDialog alert;

    public ConnectivityReceiver() {
        super();
    }

    @Override
    public void onReceive(final Context context, Intent arg1) {


        try {
            if (android.net.ConnectivityManager.CONNECTIVITY_ACTION.equals(arg1.getAction())) {

                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(context);
                alertDialog.setTitle("No Internet connection");
                alertDialog.setMessage("Please TURN ON Internet");
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton(" TURN ON", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
                            context.startActivity(intent);
                        }catch (Exception e){

                            e.printStackTrace();
                        }


                    }
                });


                assert cm != null;
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

                boolean isConnected = activeNetwork != null;
                if (isConnected) {

                    boolean state = activeNetwork.isConnected();
                    if (state) {
                        connected = 1;
                        if (alert != null) {
                            alert.dismiss();
                            alert = null;

                        }

                    }

                } else {
                    connected = 0;
                    if (alert == null) {
                        alert = alertDialog.create();
                        alert.show();
                    }
                }
            }

        }catch (Exception e){
            Log.e("connectivity error",e.toString());
            e.printStackTrace();
        }
    }



}